# Git Hooks

Scripts placed in this folder will be executed in accordance to the Git hooks policy.

To setup Git to execute the scripts in this folder you will need to run `git config core.hooksPath .githooks`

## pre-commit
