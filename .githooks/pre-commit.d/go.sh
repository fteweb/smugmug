#!/bin/bash

set -e

echo "Pre-commit for GO files"

## this will retrieve all of the .go files that have been 
## changed since the last commit
STAGED_GO_FILES=$(git diff --cached --name-only -- '*.go')

## we can check to see if this is empty
if [[ $STAGED_GO_FILES == "" ]]; then
    echo "No Go Files to Update"
    exit 0
fi

## Go Vet looks at the directory(application) as a whole
## govet is examining Go source code to find possible bugs and suspicious constructs.
go vet "$(git rev-parse --show-toplevel)/..."

## otherwise we can do stuff with these changed go files
for file in $STAGED_GO_FILES; do
    ## Handle and format imports
    ## Maunally: find "$(git rev-parse --show-toplevel)" -type f -name '*.go' -not -path "./vendor/*" -exec goimports -w {} \;
    goimports -w "${file}"
    ## format our file
    ## Maunally: find "$(git rev-parse --show-toplevel)" -type f -name '*.go' -not -path "./vendor/*" -exec gofumpt -w {} \;
    gofumpt -w "${file}"
    ## Golangci-lint is like the mother of all linters
    ## Maunally: golangci-lint run
    golangci-lint run "${file}"
    ## add any potential changes from our formatting to the 
    ## commit
    git add "${file}"
done
