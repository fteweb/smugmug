#!/bin/bash

set -e

echo "Pre-commit for Shell scripts"

## this will retrieve all of the .go files that have been 
## changed since the last commit
STAGED_SH_FILES=$(git diff --cached --name-only -- '*.sh')

## we can check to see if this is empty
if [[ "${STAGED_SH_FILES}" == "" ]]; then
    echo "No Shell scripts to Update"
    exit 0
fi

## otherwise we can do stuff with these changed go files
for file in "${STAGED_SH_FILES}"; do
    # Read first 2 bytes of file to verify shebang exists
    if IFS= LC_ALL=C read -rN2 shebang < "${file}" && [ "${shebang}" != '#!' ]; then
        echo "${file} does not have a SHEBANG"
        exit 1
    fi

    echo "Validating syntax of ${file}"
    shellcheck "${file}"
    # case "$(head -n 1 ${file})" in
    # *bash*) bash -n "${file}" ;;
    # *zsh*) zsh -n "${file}" ;;
    # *) echo "Unknown script type for ${file}"
    #    exit 1
    #    ;;
done