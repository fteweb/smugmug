# Usage

## Generate an API Access Token & Secret

Run the compiled application. `smugmug authorize` or `smugmug authorize --key <REDACTED> --secret <REDACTED>`
Enter your API Key & Secret when prompted.
Open the given URL in a browser.
Login to Smugmug with your credentials.
Enter the 6 digit code provided by Smugmug.
Copy the returned Token and Secret for use by your application.

## Other command line operations

TODO: Create this functionality and write documentation.

## Utilize the Library

TODO: Write this documentation.
