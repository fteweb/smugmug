// This package is a client to the SmugMug API v2.
// It was written to enable the population of albums and images to be worked with on local code.
// It also facilitates the upload of new images.
package main

import (
	"log"
	"os"

	smugmug "gitlab.com/fteweb/smugmug/client"
	"gitlab.com/fteweb/smugmug/client/apiv1"
)

func main() {
	client := clientSetup()
	requestURI := client.Basepath("node/" + os.Getenv("SMUGMUG_NODE_ID"))

	node, err := apiv1.GetNodeWithChildren(client, requestURI)
	if err != nil {
		client.Log.Infof("Failed to get Node with Children: %w", err)
		os.Exit(1)
	}

	// Note, you may have to process additional pages
	client.Log.Infof("Number of Children in the Node: %d\n", len(node.Response.Details.Uris.ChildNodes.Details))

	page2, err := node.Response.Details.Uris.ChildNodes.GetNextPage(client)
	if err != nil {
		client.Log.Infof("Failed to get the next page of child nodes: %w", err)
		os.Exit(1)
	}

	client.Log.Infof("Number of Children on the next page: %d\n", len(page2.Response.Details))

	page3, err := page2.Response.GetNextPage(client)
	if err != nil {
		client.Log.Infof("Failed to get the next page of child nodes: %w", err)
		os.Exit(1)
	}

	client.Log.Infof("Number of Children on the next page: %d\n", len(page3.Response.Details))

	// In my test environment this will fail as the node only has 28 items
	// var ERRPagerNotFound = fmt.Errorf("page option does not exist")
	page4, err := page3.Response.GetNextPage(client)
	if err != nil {
		client.Log.Infof("Failed to get the next page of child nodes: %w", err)
		os.Exit(1)
	}

	client.Log.Infof("Number of Children on the next page: %d\n", len(page4.Response.Details))
}

func clientSetup() *smugmug.Client {
	clientOptions := apiv1.NewClientOptions(os.Getenv("SMUGMUG_API_KEY"), os.Getenv("SMUGMUG_API_KEY_SECRET"))
	clientOptions.SetAccessToken(os.Getenv("SMUGMUG_API_TOKEN"), os.Getenv("SMUGMUG_API_TOKEN_SECRET"))
	// Get formatted JSON
	clientOptions.Pretty = true
	// Set API verbosity
	clientOptions.Verbosity = 3
	// Expansions populate inside the URI
	clientOptions.InlineExpansion = true

	client := smugmug.New(clientOptions)
	// Set Client to debug mode to log API responses
	client.Debug = true
	client.LogToDisk = true
	// Set the path to log the raw API responses
	client.ResponsePath = os.Getenv("SMUGMUG_RESPONSE_PATH")

	if err := client.Connect(); err != nil {
		log.Fatal(err)
	}

	return client
}
