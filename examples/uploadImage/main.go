// This package is a client to the SmugMug API v2.
// It was written to enable the population of albums and images to be worked with on local code.
// It also facilitates the upload of new images.
package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	smugmug "gitlab.com/fteweb/smugmug/client"
	"gitlab.com/fteweb/smugmug/client/apiv1"
)

func main() { //nolint:funlen,cyclop
	client := clientSetup()

	// Get the root of the API for the currently authenticated user
	user, err := apiv1.GetAuthenticatedUser(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user: %v", err)
		os.Exit(1)
	}

	if user.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user (API ERROR): %v", user.CheckErr())
		os.Exit(1)
	}

	// Get the Auth User's root node
	node, err := user.GetNode(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user's root node: %v", err)
		os.Exit(1)
	}

	if node.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user's root node (API ERROR): %v", node.CheckErr())
		os.Exit(1)
	}

	// Get the root node's children
	nodeChildren, err := node.GetChildren(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user's root node children: %v", err)
		os.Exit(1)
	}

	if nodeChildren.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user's root node children (API ERROR): %v", nodeChildren.CheckErr())
		os.Exit(1)
	}

	// Get the 1st childnode of the root node, should return the "test" node in my dev environment
	testNode, err := nodeChildren.GetChildNode(0)
	if err != nil {
		client.Log.Infof("Failed to get the 1st root child node [test]: %w", err)
		os.Exit(1)
	}

	// Create an album under the test node
	newAlbum, err := testNode.NewAlbum(client, fmt.Sprintf("%d", time.Now().Unix()))
	if err != nil {
		client.Log.Infof("Failed to create a new album under the [test] node: %w", err)
		os.Exit(1)
	}

	// Load the image to upload
	imageFile, err := os.Open("testImage.png")

	// Close the image file when we are done with it
	defer func() {
		if err := imageFile.Close(); err != nil && errors.Is(err, os.ErrClosed) {
			client.Log.Infof("Failed to close the test image: %w", err)
		}
	}()

	// Check if we failed to load the image
	if err != nil {
		client.Log.Infof("Failed to load the test image: %w", err)

		if err := imageFile.Close(); err != nil && errors.Is(err, os.ErrClosed) {
			client.Log.Infof("Failed to close the test image: %w", err)
		}

		os.Exit(1) //nolint:gocritic
	}

	// Upload the image
	result, err := newAlbum.UploadImage(client, imageFile, fmt.Sprintf("%d", time.Now().Unix()))
	if err != nil {
		client.Log.Infof("Failed to upload the test image: %w", err)

		if err := imageFile.Close(); err != nil && errors.Is(err, os.ErrClosed) {
			client.Log.Infof("Failed to close the test image: %w", err)
		}

		os.Exit(1)
	}

	_ = result
}

func clientSetup() *smugmug.Client {
	clientOptions := apiv1.NewClientOptions(os.Getenv("SMUGMUG_API_KEY"), os.Getenv("SMUGMUG_API_KEY_SECRET"))
	clientOptions.SetAccessToken(os.Getenv("SMUGMUG_API_TOKEN"), os.Getenv("SMUGMUG_API_TOKEN_SECRET"))
	// Get formatted JSON
	clientOptions.Pretty = true
	// Set API verbosity
	clientOptions.Verbosity = 3
	// Expansions populate inside the URI
	clientOptions.InlineExpansion = true

	client := smugmug.New(clientOptions)
	// Set Client to debug mode to log API responses
	client.Debug = true
	client.LogToDisk = true
	// Set the path to log the raw API responses
	client.ResponsePath = os.Getenv("SMUGMUG_RESPONSE_PATH")

	if err := client.Connect(); err != nil {
		log.Fatal(err)
	}

	return client
}
