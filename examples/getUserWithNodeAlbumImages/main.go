// This package is a client to the SmugMug API v2.
// It was written to enable the population of albums and images to be worked with on local code.
// It also facilitates the upload of new images.
package main

import (
	"log"
	"os"

	smugmug "gitlab.com/fteweb/smugmug/client"
	"gitlab.com/fteweb/smugmug/client/apiv1"
)

func main() { //nolint:funlen,cyclop
	client := clientSetup()

	user, err := apiv1.GetAuthenticatedUser(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user: %v", err)
		os.Exit(1)
	}

	if user.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user (API ERROR): %v", user.CheckErr())
		os.Exit(1)
	}

	node, err := user.GetNode(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user's root node: %v", err)
		os.Exit(1)
	}

	if node.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user's root node (API ERROR): %v", node.CheckErr())
		os.Exit(1)
	}

	nodeChildren, err := node.GetChildren(client)
	if err != nil {
		client.Log.Infof("Failed to get authenticated user's root node children: %v", err)
		os.Exit(1)
	}

	if nodeChildren.CheckErr() != nil {
		client.Log.Infof("Failed to get authenticated user's root node children (API ERROR): %v", nodeChildren.CheckErr())
		os.Exit(1)
	}

	// Save the 1st node child
	testNode, err := nodeChildren.GetChildNode(0)
	if err != nil {
		client.Log.Infof("Failed to get the 1st root child node [test]: %w", err)
		os.Exit(1)
	}

	testNodeChildren, err := testNode.GetChildren(client)
	if err != nil {
		client.Log.Infof("Failed to get the test node's children: %w", err)
		os.Exit(1)
	}

	// Save the 1st node child
	testNodeChild, err := testNodeChildren.GetChildNode(0)
	if err != nil {
		client.Log.Infof("Failed to get the 1st test child node: %w", err)
		os.Exit(1)
	}

	album, err := testNodeChild.GetAlbum(client)
	if err != nil {
		client.Log.Infof("Failed to get the 1st root child node's album: %v", err)
		os.Exit(1)
	}

	if album.CheckErr() != nil {
		client.Log.Infof("Failed to get the 1st root child node's album (API ERROR): %v", album.CheckErr())
		os.Exit(1)
	}

	albumImages, err := album.GetImages(client)
	if err != nil {
		client.Log.Infof("Failed to get the album's images: %v", err)
		os.Exit(1)
	}

	if albumImages.CheckErr() != nil {
		client.Log.Infof("Failed to get the album's images (API ERROR): %v", albumImages.CheckErr())
		os.Exit(1)
	}

	firstImage, err := albumImages.GetImage(0)
	if err != nil {
		client.Log.Infof("Failed to get the album's first image: %w", err)
		os.Exit(1)
	}

	imageSizes, err := firstImage.GetSizes(client)
	if err != nil {
		client.Log.Infof("Failed to get the first image's available sizes: %v", err)
		os.Exit(1)
	}

	if imageSizes.CheckErr() != nil {
		client.Log.Infof("Failed to get the first image's available sizes (API ERROR): %v", imageSizes.CheckErr())
		os.Exit(1)
	}

	client.Log.Infof("Available Image Sizes: %+v\n", imageSizes.Response.Details)
}

func clientSetup() *smugmug.Client {
	clientOptions := apiv1.NewClientOptions(os.Getenv("SMUGMUG_API_KEY"), os.Getenv("SMUGMUG_API_KEY_SECRET"))
	clientOptions.SetAccessToken(os.Getenv("SMUGMUG_API_TOKEN"), os.Getenv("SMUGMUG_API_TOKEN_SECRET"))
	// Get formatted JSON
	clientOptions.Pretty = true
	// Set API verbosity
	clientOptions.Verbosity = 3
	// Expansions populate inside the URI
	clientOptions.InlineExpansion = true

	client := smugmug.New(clientOptions)
	// Set Client to debug mode to log API responses
	client.Debug = true
	client.LogToDisk = true
	// Set the path to log the raw API responses
	client.ResponsePath = os.Getenv("SMUGMUG_RESPONSE_PATH")

	if err := client.Connect(); err != nil {
		log.Fatal(err)
	}

	return client
}
