// This package is a client to the SmugMug API v2.
// It was written to enable the population of albums and images to be worked with on local code.
// It also facilitates the upload of new images.
package main

import (
	"log"
	"os"

	smugmug "gitlab.com/fteweb/smugmug/client"
	"gitlab.com/fteweb/smugmug/client/apiv1"
)

func main() {
	client := clientSetup()
	requestURI := client.Basepath("album/" + os.Getenv("SMUGMUG_ALBUM_ID") + "!images")

	albumImages, err := apiv1.GetAlbumImages(client, requestURI, nil)
	if err != nil {
		client.Log.Infof("Failed to get Album Images: %w", err)
		os.Exit(1)
	}

	client.Log.Infof("Number of images in the Album: %d\n", len(albumImages.Response.Details))
}

func clientSetup() *smugmug.Client {
	clientOptions := apiv1.NewClientOptions(os.Getenv("SMUGMUG_API_KEY"), os.Getenv("SMUGMUG_API_KEY_SECRET"))
	clientOptions.SetAccessToken(os.Getenv("SMUGMUG_API_TOKEN"), os.Getenv("SMUGMUG_API_TOKEN_SECRET"))
	// Get formatted JSON
	clientOptions.Pretty = true
	// Set API verbosity
	clientOptions.Verbosity = 3
	// Expansions populate inside the URI
	clientOptions.InlineExpansion = true

	client := smugmug.New(clientOptions)
	// Set Client to debug mode to log API responses
	client.Debug = true
	client.LogToDisk = true
	// Set the path to log the raw API responses
	client.ResponsePath = os.Getenv("SMUGMUG_RESPONSE_PATH")

	if err := client.Connect(); err != nil {
		log.Fatal(err)
	}

	return client
}
