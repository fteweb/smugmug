# Load .env from the git root

```bash
set -o allexport
source "$(git rev-parse --show-toplevel)/.env"
set +o allexport
```
