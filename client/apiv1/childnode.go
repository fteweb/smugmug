package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeChildNode returns an initialized SmugMug child node configuration.
func DecodeChildNode(clt client, data []byte) (*ChildNode, error) {
	endpoint := new(ChildNode)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetChildNode will return the child node at the specified URI.
func GetChildNode(clt client, uri string, apiConfig *APIConfig) (*ChildNode, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [ChildNode] resource: %w", err)
	}

	return DecodeChildNode(clt, resourceData)
}

type ChildNode struct {
	// resource Template
	*metadata.Base
	// sub-struct
	Response childNodeResponse `json:"response,omitempty"`
}

func (e *ChildNode) endpointType() EndpointType {
	return EndpointChildNode
}

var ErrInvalidChildNode = fmt.Errorf("invalid child node index")

func (e *ChildNode) GetChildNode(nodeIndex int) (*NodeDetails, error) {
	if len(e.Response.Details) >= nodeIndex {
		return e.Response.Details[nodeIndex], nil
	}

	return nil, ErrInvalidChildNode
}

func (e *ChildNode) GetAlbum(c client, nodeIndex int) (*Album, error) {
	if len(e.Response.Details) >= nodeIndex {
		return e.Response.Details[nodeIndex].GetAlbum(c)
	}

	return nil, ErrInvalidChildNode
}

type childNodeResponse struct {
	*metadata.Response
	Details []*NodeDetails `json:"node,omitempty"`
	// List of sub-resources
	// Uris nodeUris `json:"Uris"`
}

func (u *childNodeResponse) GetFirstPage(c client) (*ChildNode, error) {
	if u.Pages.FirstPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.FirstPage, nil)
}

func (u *childNodeResponse) GetLastPage(c client) (*ChildNode, error) {
	if u.Pages.LastPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.LastPage, nil)
}

func (u *childNodeResponse) GetPrevPage(c client) (*ChildNode, error) {
	if u.Pages.PrevPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.PrevPage, nil)
}

func (u *childNodeResponse) GetNextPage(c client) (*ChildNode, error) {
	if u.Pages.NextPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.NextPage, nil)
}
