package apiv1

import (
	"encoding/json"
	"fmt"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

type (
	nodePrivacyString       string
	nodePrivacyInt          int
	nodeSecurityTypeString  string
	nodeSecurityTypeInt     int
	nodeSearchableString    string
	nodeSearchableInt       int
	nodeSortDirectionString string
	nodeSortDirectionInt    int
	nodeSortMethodString    string
	nodeSortMethodInt       int
	nodeTypeString          string
	nodeTypeInt             int
)

const (
	private                nodePrivacyString       = "Private"
	privateInt             nodePrivacyInt          = 1
	unlisted               nodePrivacyString       = "Unlisted"
	unlistedInt            nodePrivacyInt          = 2
	public                 nodePrivacyString       = "Public"
	publicInt              nodePrivacyInt          = 3
	securityNone           nodeSecurityTypeString  = "None"
	securityNoneInt        nodeSecurityTypeInt     = 1
	securityPassword       nodeSecurityTypeString  = "Password"
	securityPasswordInt    nodeSecurityTypeInt     = 2
	securityGrantAccess    nodeSecurityTypeString  = "GrantAccess"
	securityGrantAccessInt nodeSecurityTypeInt     = 3
	searchableNo           nodeSearchableString    = "No"
	searchableNoInt        nodeSearchableInt       = 0
	searchableInherit      nodeSearchableString    = "Inherit from User"
	searchableInheritInt   nodeSearchableInt       = 1
	searchableHome         nodeSearchableString    = "HomeOnly"
	searchableHomeInt      nodeSearchableInt       = 2
	searchableYes          nodeSearchableString    = "Yes"
	searchableYesInt       nodeSearchableInt       = 3
	searchableLocalUser    nodeSearchableString    = "LocalUser"
	searchableLocalUserInt nodeSearchableInt       = 6
	searchableLocal        nodeSearchableString    = "Local"
	searchableLocalInt     nodeSearchableInt       = 7
	sortAscending          nodeSortDirectionString = "Ascending"
	sortAscendingInt       nodeSortDirectionInt    = 1
	sortDescending         nodeSortDirectionString = "Descending"
	sortDescendingInt      nodeSortDirectionInt    = 2
	sortByIndex            nodeSortMethodString    = "SortIndex"
	sortByIndexInt         nodeSortMethodInt       = 2
	sortByName             nodeSortMethodString    = "Name"
	sortByNameInt          nodeSortMethodInt       = 3
	sortByAdded            nodeSortMethodString    = "DateAdded"
	sortByAddedInt         nodeSortMethodInt       = 5
	sortByModified         nodeSortMethodString    = "DateModified"
	sortByModifiedInt      nodeSortMethodInt       = 6
	NodeTypeFolder         nodeTypeString          = "Folder"
	NodeTypeFolderInt      nodeTypeInt             = 2
	NodeTypeAlbum          nodeTypeString          = "Album"
	NodeTypeAlbumInt       nodeTypeInt             = 4
	NodeTypePage           nodeTypeString          = "Page"
	NodeTypePageInt        nodeTypeInt             = 64
)

type nodeConfig struct {
	// Whether to automatically generate a unique UrlName if the given UrlName already exists.
	AutoRename bool
	// Link to the cover image
	CoverImageURI *string `json:"-"`
	// Human-readable description for this node. May contain basic HTML.
	// Some node types display this to the user; some merely use it for search engine optimization.
	Description string `json:"-"`
	// Whether to hide the identity of this node's owner from visitors. Only applicable to Album nodes.
	HideOwner bool `json:"-"`
	// link to a highlight image
	HighlightImageURI *string `json:"-"`
	// Array of keywords describing the content of this node. Some node types use this for search engine optimization.
	Keywords []string `json:"-"`
	// Human-readable name for this node.
	Name string
	// Album password
	Password string `json:"-"`
	// The hint for the album-viewing password
	PasswordHint string `json:"-"`
	// Privacy level for this node.
	// NOTE: This may be overridden by a more restrictive privacy setting inherited from one of this node's ancestors.
	// See EffectivePrivacy.
	//
	// Private, Unlisted, or Public
	privacy nodePrivacyString
	Privacy nodePrivacyInt
	// Security type for this node.
	// NOTE: This may be overridden by a more restrictive security setting inherited from one of this node's ancestors.
	// See EffectiveSecurityType.
	securityType nodeSecurityTypeString
	SecurityType nodeSecurityTypeInt
	// Whether or not to display the cover image when possible.
	ShowCoverImage bool `json:"-"`
	// Allow this album to appear in SmugMug search results? Can be "No" or "Inherit from User".
	//
	// Acceptable values differ for root nodes and child nodes.
	//
	// Root nodes: No, Local, LocalUser, Yes
	//
	// Child nodes: No, Inherit from User
	smugSearchable nodeSearchableString
	SmugSearchable nodeSearchableInt
	// Content sort direction
	sortDirection nodeSortDirectionString
	SortDirection nodeSortDirectionInt
	// What property to sort by
	sortMethod nodeSortMethodString
	SortMethod nodeSortMethodInt
	// Type of the node
	nodeType nodeTypeString
	Type     nodeTypeInt
	// This is usually a URL-friendly version of the human-readable name. Must start with a capital letter.
	URLName string `json:"-"`
	// Allow this album to appear in external search results? Can be "No" or "Inherit from User".
	//
	// Acceptable values differ for root nodes and child nodes.
	//
	// Root nodes: No, HomeOnly, Yes
	//
	// Child nodes: No, Inherit from User
	worldSearchable nodeSearchableString
	WorldSearchable nodeSearchableInt
}

func (nc *nodeConfig) AddKeyword(keyword string) {
	nc.Keywords = append(nc.Keywords, keyword)
}

func (nc *nodeConfig) SetPrivate() {
	nc.privacy = private
}

func (nc *nodeConfig) SetUnlisted() {
	nc.privacy = unlisted
}

func (nc *nodeConfig) SetPublic() {
	nc.privacy = public
}

func (nc *nodeConfig) RemoveSecurity() {
	nc.securityType = securityNone
}

func (nc *nodeConfig) EnablePassword() {
	nc.securityType = securityPassword
}

func (nc *nodeConfig) GrantSecurity() {
	nc.securityType = securityGrantAccess
}

func (nc *nodeConfig) SortAscending() {
	nc.sortDirection = sortAscending
}

func (nc *nodeConfig) SortDescending() {
	nc.sortDirection = sortDescending
}

func (nc *nodeConfig) SortByDateModified() {
	nc.sortMethod = sortByModified
}

func (nc *nodeConfig) SortByDateAdded() {
	nc.sortMethod = sortByAdded
}

func (nc *nodeConfig) SortByIndex() {
	nc.sortMethod = sortByIndex
}

func (nc *nodeConfig) SortByName() {
	nc.sortMethod = sortByName
}

var (
	ErrNodePrivacy       = fmt.Errorf("invalid privacy mode")
	ErrNodeSecurity      = fmt.Errorf("invalid security mode")
	ErrNodeSearchSmugmug = fmt.Errorf("invalid smugmug search mode")
	ErrNodeType          = fmt.Errorf("invalid node type")
	ErrNodeSearchWorld   = fmt.Errorf("invalid world search mode")
)

func (nc *nodeConfig) Verify() error {
	// Check privacy setting
	if err := nc.verifyPrivacy(); err != nil {
		return err
	}

	// Check security type
	if err := nc.verifySecurity(); err != nil {
		return err
	}

	// check SmugMug searchable
	if err := nc.verifySearchableSmugmug(); err != nil {
		return err
	}

	// check World searchable
	if err := nc.verifySearchableWorld(); err != nil {
		return err
	}

	// check node type
	if err := nc.verifyNodeType(); err != nil {
		return err
	}

	// check sort direction
	nc.verifySortDirection()

	// check sort method
	nc.verifySortMethod()

	// make sure that the URL name is title case
	nc.URLName = cases.Title(language.AmericanEnglish, cases.NoLower).String(nc.URLName)

	return nil
}

func (nc *nodeConfig) verifyPrivacy() error {
	// Check privacy setting
	switch nc.privacy {
	case private:
		nc.Privacy = privateInt

		return nil
	case unlisted:
		nc.Privacy = unlistedInt

		return nil
	case public:
		nc.Privacy = publicInt

		return nil
	case "":
		nc.Privacy = 0

		return nil
	default:
		return ErrNodePrivacy
	}
}

func (nc *nodeConfig) verifySecurity() error {
	// Check security type
	switch nc.securityType {
	case securityNone:
		nc.SecurityType = securityNoneInt

		return nil
	case securityPassword:
		nc.SecurityType = securityPasswordInt

		return nil
	case securityGrantAccess:
		nc.SecurityType = securityGrantAccessInt

		return nil
	case "":
		nc.SecurityType = securityPasswordInt

		return nil
	default:
		return ErrNodeSecurity
	}
}

func (nc *nodeConfig) verifySearchableSmugmug() error {
	switch nc.smugSearchable {
	case searchableYes:
		nc.SmugSearchable = searchableYesInt

		return nil
	case searchableNo:
		nc.SmugSearchable = searchableNoInt

		return nil
	case searchableHome:
		nc.SmugSearchable = searchableHomeInt

		return nil
	case searchableLocal:
		nc.SmugSearchable = searchableLocalInt

		return nil
	case searchableLocalUser:
		nc.SmugSearchable = searchableLocalUserInt

		return nil
	case searchableInherit:
		nc.SmugSearchable = searchableInheritInt

		return nil
	case "":
		nc.SmugSearchable = searchableInheritInt

		return nil
	default:
		return ErrNodeSearchSmugmug
	}
}

func (nc *nodeConfig) verifySearchableWorld() error {
	// check world searchable
	switch nc.worldSearchable {
	case searchableYes:
		nc.WorldSearchable = searchableYesInt

		return nil
	case searchableNo:
		nc.WorldSearchable = searchableNoInt

		return nil
	case searchableHome:
		nc.WorldSearchable = searchableHomeInt

		return nil
	case searchableLocal:
		nc.WorldSearchable = searchableLocalInt

		return nil
	case searchableLocalUser:
		nc.WorldSearchable = searchableLocalUserInt

		return nil
	case searchableInherit:
		nc.WorldSearchable = searchableInheritInt

		return nil
	case "":
		nc.WorldSearchable = searchableInheritInt

		return nil
	default:
		return ErrNodeSearchWorld
	}
}

func (nc *nodeConfig) verifyNodeType() error {
	switch nc.nodeType {
	case NodeTypeAlbum:
		nc.Type = NodeTypeAlbumInt

		return nil
	case NodeTypeFolder:
		nc.Type = NodeTypeFolderInt

		return nil
	case NodeTypePage:
		nc.Type = NodeTypePageInt

		return nil
	default:
		return ErrNodeType
	}
}

func (nc *nodeConfig) verifySortDirection() {
	// check sort direction
	switch nc.sortDirection {
	case sortAscending:
		nc.SortDirection = sortAscendingInt
	case sortDescending:
		nc.SortDirection = sortDescendingInt
	default:
		nc.SortDirection = sortAscendingInt
	}
}

func (nc *nodeConfig) verifySortMethod() {
	// check sort method
	switch nc.sortMethod {
	case sortByAdded:
		nc.SortMethod = sortByAddedInt
	case sortByIndex:
		nc.SortMethod = sortByIndexInt
	case sortByModified:
		nc.SortMethod = sortByModifiedInt
	case sortByName:
		nc.SortMethod = sortByNameInt
	default:
		nc.SortMethod = sortByAddedInt
	}
}

func (nc *nodeConfig) Create() ([]byte, error) {
	if err := nc.Verify(); err != nil {
		return nil, fmt.Errorf("failed to verify node config: %w", err)
	}

	nodeBytes, err := json.MarshalIndent(nc, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("failed to Marshal Node Config JSON: %w", err)
	}

	return nodeBytes, nil
}
