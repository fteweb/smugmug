package apiv1

// NewChildNodeConfig returns an initialized SmugMug child node configuration.
func NewChildNodeConfig(name string) *ChildNodeConfig {
	node := new(ChildNodeConfig)
	node.Name = name
	node.nodeType = NodeTypeFolder
	node.SetUnlisted()
	node.RemoveSecurity()
	node.SetSmugSearchableInherit()
	node.SetWorldSearchableInherit()
	node.AutoRename = true

	return node
}

type ChildNodeConfig struct {
	nodeConfig
}

func (ac *ChildNodeConfig) SetSmugSearchableNo() {
	ac.smugSearchable = searchableNo
}

func (ac *ChildNodeConfig) SetSmugSearchableInherit() {
	ac.smugSearchable = searchableInherit
}

func (ac *ChildNodeConfig) SetWorldSearchableNo() {
	ac.worldSearchable = searchableNo
}

func (ac *ChildNodeConfig) SetWorldSearchableInherit() {
	ac.worldSearchable = searchableInherit
}

func (ac *ChildNodeConfig) Verify() error {
	return ac.nodeConfig.Verify()
}
