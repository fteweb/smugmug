package apiv1

import (
	"fmt"
	"io"
	"time"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeAlbum returns a new instance of a SmugMug Album from the provided client response.
func DecodeAlbum(clt client, data []byte) (*Album, error) {
	endpoint := new(Album)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetAlbum will return the album at the specified URI.
func GetAlbum(clt client, uri string, apiConfig *APIConfig) (*Album, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [Album] resource: %w", err)
	}

	return DecodeAlbum(clt, resourceData)
}

// GetAlbumWithImages returns an album struct with images and image sizes.
func GetAlbumWithImages(clt client, albumURI string) (*Album, error) {
	// Configure API request to populate the Album with Images
	apiConfig := NewAPIConfig()
	apiConfig.Enable = true
	apiConfig.AppendFilterURI("AlbumImages")
	albumImages := apiConfig.NewExpansion("AlbumImages")
	albumImages.AppendFilterURI("ImageSizes")
	albumImages.NewExpansion("ImageSizes")

	return GetAlbum(clt, albumURI, apiConfig)
}

type Album struct {
	// resource Template
	*metadata.Base
	// sub-struct
	Response albumResponse `json:"response,omitempty"`
}

func (e *Album) endpointType() EndpointType {
	return EndpointAlbum
}

func (e *Album) GetImages(c client) (*AlbumImages, error) {
	return e.Response.Details.Uris.AlbumImages.Get(c)
}

// UploadImage will upload a given reader
//
// Reader should be seeked to start prior to passing if it is a file
// `_, err = file.Seek(0, 0)`.
func (e *Album) UploadImage(clt client, image io.Reader, name string) (*UploadResponse, error) {
	// TODO: Verify image is valid
	// Configure headers
	uploadConfig := NewUpload(e.Response.URI)
	uploadConfig.FileName = name
	uploadConfig.Pretty = true

	// Upload the image
	response, err := clt.UploadResource(image, uploadConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to upload resource: %w", err)
	}

	uploadResponse, err := NewUploadResponse(response)
	if err != nil {
		return nil, fmt.Errorf("failed to decode the upload response: %w", err)
	}

	return uploadResponse, nil
}

/*
{
    "Uri":"/api/v2/user",
    "Uris":{  },
    "UriDescription":"Base URI for user-related operations",
    "EndpointType":"UserBase",
    "DocUri":"https://api.smugmug.com/api/v2/doc/reference/user.html",
    "Timing":{  }
}
*/
// AlbumResponse contains the response from the album API Endpoint.
type albumResponse struct {
	*metadata.Response
	Details AlbumDetails `json:"album,omitempty"`
	// List of sub-resources
	// Uris nodeUris `json:"Uris"`
}

// AlbumDetails is the actual endpoint.
type AlbumDetails struct {
	NiceName               string    `json:"niceName,omitempty"`
	URLName                string    `json:"urlName,omitempty"`
	Title                  string    `json:"title,omitempty"`
	Name                   string    `json:"name,omitempty"`
	AllowDownloads         bool      `json:"allowDownloads,omitempty"`
	Backprinting           string    `json:"backprinting,omitempty"`
	BoutiquePackaging      string    `json:"boutiquePackaging,omitempty"`
	CanRank                bool      `json:"canRank,omitempty"`
	Clean                  bool      `json:"clean,omitempty"`
	Comments               bool      `json:"comments,omitempty"`
	Description            string    `json:"description,omitempty"`
	EXIF                   bool      `json:"exif,omitempty"`
	External               bool      `json:"external,omitempty"`
	FamilyEdit             bool      `json:"familyEdit,omitempty"`
	Filenames              bool      `json:"filenames,omitempty"`
	FriendEdit             bool      `json:"friendEdit,omitempty"`
	Geography              bool      `json:"geography,omitempty"`
	Header                 string    `json:"header,omitempty"`
	HideOwner              bool      `json:"hideOwner,omitempty"`
	InterceptShipping      string    `json:"interceptShipping,omitempty"`
	Keywords               string    `json:"keywords,omitempty"`
	LargestSize            string    `json:"largestSize,omitempty"`
	PackagingBranding      bool      `json:"packagingBranding,omitempty"`
	Password               string    `json:"password,omitempty"`
	PasswordHint           string    `json:"passwordHint,omitempty"`
	Printable              bool      `json:"printable,omitempty"`
	Privacy                string    `json:"privacy,omitempty"`
	ProofDays              int       `json:"proofDays,omitempty"`
	ProofDigital           bool      `json:"proofDigital,omitempty"`
	Protected              bool      `json:"protected,omitempty"`
	Share                  bool      `json:"share,omitempty"`
	Slideshow              bool      `json:"slideshow,omitempty"`
	SmugSearchable         string    `json:"smugSearchable,omitempty"`
	SortDirection          string    `json:"sortDirection,omitempty"`
	SortMethod             string    `json:"sortMethod,omitempty"`
	SquareThumbs           bool      `json:"squareThumbs,omitempty"`
	Watermark              bool      `json:"watermark,omitempty"`
	WorldSearchable        bool      `json:"worldSearchable,omitempty"`
	SecurityType           string    `json:"securityType,omitempty"`
	HighlightAlbumImageURI string    `json:"highlightAlbumImageUri,omitempty"`
	AlbumKey               string    `json:"albumKey,omitempty"`
	CanBuy                 bool      `json:"canBuy,omitempty"`
	CanFavorite            bool      `json:"canFavorite,omitempty"`
	Date                   time.Time `json:"date,omitempty"`
	LastUpdated            time.Time `json:"lastUpdated,omitempty"`
	ImagesLastUpdated      time.Time `json:"imagesLastUpdated,omitempty"`
	NodeID                 string    `json:"nodeID,omitempty"` //nolint:tagliatelle
	OriginalSizes          int       `json:"originalSizes,omitempty"`
	TotalSizes             int       `json:"totalSizes,omitempty"`
	ImageCount             int       `json:"imageCount,omitempty"`
	URLPath                string    `json:"urlPath,omitempty"`
	CanShare               bool      `json:"canShare,omitempty"`
	HasDownloadPassword    bool      `json:"hasDownloadPassword,omitempty"`
	Packages               bool      `json:"packages,omitempty"`
	URI                    string    `json:"uri,omitempty"`
	WebURI                 string    `json:"webUri,omitempty"`
	URIDescription         string    `json:"uriDescription,omitempty"`
	Uris                   albumUris `json:"uris,omitempty"`
	ResponseLevel          string    `json:"responseLevel,omitempty"`
}

type albumUris struct {
	AlbumShareUris             albumShareUrisURI             `json:"albumShareUris"`
	Node                       nodeURI                       `json:"node"`
	NodeCoverImage             nodeCoverImageURI             `json:"nodeCoverImage"`
	User                       subUserURI                    `json:"user"`
	Folder                     folderURI                     `json:"folder"`
	ParentFolders              parentFoldersURI              `json:"parentFolders"`
	HighlightImage             highlightImageURI             `json:"highlightImage"`
	AlbumHighlightImage        albumHighlightImageURI        `json:"albumHighlightImage"`
	AlbumImages                albumImagesURI                `json:"albumImages"`
	AlbumPopularMedia          albumPopularMediaURI          `json:"albumPopularMedia"`
	AlbumGeoMedia              albumGeoMediaURI              `json:"albumGeoMedia"`
	AlbumComments              albumCommentsURI              `json:"albumComments"`
	MoveAlbumImages            moveAlbumImagesURI            `json:"moveAlbumImages"`
	CollectImages              collectImagesURI              `json:"collectImages"`
	ApplyAlbumTemplate         applyAlbumTemplateURI         `json:"applyAlbumTemplate"`
	DeleteAlbumImages          deleteAlbumImagesURI          `json:"deleteAlbumImages"`
	UploadFromExternalResource uploadFromExternalResourceURI `json:"uploadFromExternalResource"`
	UploadFromURI              uploadFromURI                 `json:"uploadFromUri"`
	AlbumDownload              albumDownloadURI              `json:"albumDownload"`
	AlbumPrices                albumPricesURI                `json:"albumPrices"`
	AlbumPricelistExclusions   albumPricelistExclusionsURI   `json:"albumPricelistExclusions"`
	SortAlbumImages            sortAlbumImagesURI            `json:"sortAlbumImages"`
}
