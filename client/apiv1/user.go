package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

const EndpointAuthUser = "!authuser"

// Return a new instance of a user API struct.
func DecodeUser(clt client, data []byte) (*User, error) {
	endpoint := new(User)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetUser will return the user at the specified URI.
func GetUser(clt client, uri string, apiConfig *APIConfig) (*User, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [User] resource: %w", err)
	}

	return DecodeUser(clt, resourceData)
}

// GetAuthenticatedUser will return the user that owns the API authentication token.
func GetAuthenticatedUser(c client) (*User, error) {
	return GetUser(c, c.Basepath(EndpointAuthUser), nil)
}

// GetAuthenticatedUserWithNode will return the user that owns the API authentication token and their root node.
func GetAuthenticatedUserWithNode(clt client) (*User, error) {
	// Configure API request to populate the user's root Node.
	apiConfig := NewAPIConfig()
	apiConfig.Enable = true
	apiConfig.AppendFilterURI("Node").NewExpansion("Node")

	return GetUser(clt, clt.Basepath(EndpointAuthUser), apiConfig)
}

// GetAuthenticatedUserWithAlbums will return the user that owns the API authentication token and their albums.
func GetAuthenticatedUserWithAlbums(clt client) (*User, error) {
	// Configure API request to populate the user's root Node.
	apiConfig := NewAPIConfig()
	apiConfig.Enable = true
	apiConfig.AppendFilterURI("UserAlbums").NewExpansion("UserAlbums")

	return GetUser(clt, clt.Basepath(EndpointAuthUser), apiConfig)
}

type User struct {
	*metadata.Base
	Response userResponse `json:"response,omitempty"`
}

func (e *User) endpointType() EndpointType {
	return EndpointUser
}

func (e *User) GetNode(c client) (*Node, error) {
	return e.Response.Details.Uris.Node.Get(c)
}

type userResponse struct {
	*metadata.Response
	Details userDetails `json:"user,omitempty"`
}
type userDetails struct {
	AccountStatus    string   `json:"accountStatus,omitempty"`
	FirstName        string   `json:"firstName,omitempty"`
	FriendsView      bool     `json:"friendsView,omitempty"`
	ImageCount       int      `json:"imageCount,omitempty"`
	IsGettingStarted bool     `json:"isGettingStarted,omitempty"`
	IsTrial          bool     `json:"isTrial,omitempty"`
	LastName         string   `json:"lastName,omitempty"`
	NickName         string   `json:"nickName,omitempty"`
	SortBy           string   `json:"sortBy,omitempty"`
	ViewPassHint     string   `json:"viewPassHint,omitempty"`
	ViewPassword     string   `json:"viewPassword,omitempty"`
	Domain           string   `json:"domain,omitempty"`
	DomainOnly       string   `json:"domainOnly,omitempty"`
	RefTag           string   `json:"refTag,omitempty"`
	Name             string   `json:"name,omitempty"`
	Plan             string   `json:"plan,omitempty"`
	QuickShare       bool     `json:"quickShare,omitempty"`
	URI              string   `json:"uri,omitempty"`
	WebURI           string   `json:"webUri,omitempty"`
	URIDescription   string   `json:"uriDescription,omitempty"`
	Uris             userUris `json:"uris,omitempty"`
	ResponseLevel    string   `json:"responseLevel,omitempty"`
}

type userUris struct {
	BioImage                 bioImageURI                 `json:"bioImage,omitempty"`
	CoverImage               coverImageURI               `json:"coverImage,omitempty"`
	UserProfile              userProfileURI              `json:"userProfile,omitempty"`
	Node                     nodeURI                     `json:"node,omitempty"`
	Folder                   folderURI                   `json:"folder,omitempty"`
	Features                 featuresURI                 `json:"features,omitempty"`
	SiteSettings             siteSettingsURI             `json:"siteSettings,omitempty"`
	UserAlbums               userAlbumsURI               `json:"userAlbums,omitempty"`
	UserGeoMedia             userGeoMediaURI             `json:"userGeoMedia,omitempty"`
	UserPopularMedia         userPopularMediaURI         `json:"userPopularMedia,omitempty"`
	UserFeaturedAlbums       userFeaturedAlbumsURI       `json:"userFeaturedAlbums,omitempty"`
	UserRecentImages         userRecentImagesURI         `json:"userRecentImages,omitempty"`
	UserImageSearch          userImageSearchURI          `json:"userImageSearch,omitempty"`
	UserTopKeywords          userTopKeywordsURI          `json:"userTopKeywords,omitempty"`
	URLPathLookup            urlPathLookupURI            `json:"urlPathLookup,omitempty"`
	UserAlbumTemplates       userAlbumTemplatesURI       `json:"userAlbumTemplates,omitempty"`
	SortUserFeaturedAlbums   sortUserFeaturedAlbumsURI   `json:"sortUserFeaturedAlbums,omitempty"`
	UserTasks                userTasksURI                `json:"userTasks,omitempty"`
	UserWatermarks           userWatermarksURI           `json:"userWatermarks,omitempty"`
	UserPrintmarks           userPrintmarksURI           `json:"userPrintmarks,omitempty"`
	UserUploadLimits         userUploadLimitsURI         `json:"userUploadLimits,omitempty"`
	UserAssetsAlbum          userAssetsAlbumURI          `json:"userAssetsAlbum,omitempty"`
	UserLatestQuickNews      userLatestQuickNewsURI      `json:"userLatestQuickNews,omitempty"`
	UserGuideStates          userGuideStatesURI          `json:"userGuideStates,omitempty"`
	UserHideGuides           userHideGuidesURI           `json:"userHideGuides,omitempty"`
	UserGrants               userGrantsURI               `json:"userGrants,omitempty"`
	DuplicateImageSearch     duplicateImageSearchURI     `json:"duplicateImageSearch,omitempty"`
	UserDeletedAlbums        userDeletedAlbumsURI        `json:"userDeletedAlbums,omitempty"`
	UserDeletedFolders       userDeletedFoldersURI       `json:"userDeletedFolders,omitempty"`
	UserDeletedPages         userDeletedPagesURI         `json:"userDeletedPages,omitempty"`
	UserContacts             userContactsURI             `json:"userContacts,omitempty"`
	RawManagementAddOnStatus rawManagementAddOnStatusURI `json:"rawManagementAddOnStatus,omitempty"`
}
