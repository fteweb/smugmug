package apiv1

// NewAlbumConfig returns an initialized SmugMug album configuration.
func NewAlbumConfig(name string) *AlbumConfig {
	node := new(AlbumConfig)
	node.Name = name
	node.nodeType = NodeTypeAlbum
	node.SetUnlisted()
	node.RemoveSecurity()
	node.SetSmugSearchableInherit()
	node.SetWorldSearchableInherit()
	node.AutoRename = true

	return node
}

type AlbumConfig struct {
	nodeConfig
}

func (ac *AlbumConfig) SetSmugSearchableNo() {
	ac.smugSearchable = searchableNo
}

func (ac *AlbumConfig) SetSmugSearchableInherit() {
	ac.smugSearchable = searchableInherit
}

func (ac *AlbumConfig) SetWorldSearchableNo() {
	ac.worldSearchable = searchableNo
}

func (ac *AlbumConfig) SetWorldSearchableInherit() {
	ac.worldSearchable = searchableInherit
}

func (ac *AlbumConfig) Verify() error {
	return ac.nodeConfig.Verify()
}
