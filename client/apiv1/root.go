package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeSmugMug returns a new instance of the root SmugMug API struct.
func DecodeSmugMug(clt client, data []byte) (*SmugMug, error) {
	endpoint := new(SmugMug)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetSmugMug will return the root API response.
func GetSmugMug(clt client) (*SmugMug, error) {
	resourceData, err := clt.GetResource(clt.Basepath(""), NewAPIConfig())
	if err != nil {
		return nil, clt.Errorf("failed to get [API Root] resource: %w", err)
	}

	return DecodeSmugMug(clt, resourceData)
}

// The SmugMug API is designed to be discoverable and self-documenting,
// which means you can learn a lot about it just navigating the API itself using our Live API Browser.
// However, we have not yet perfected the self-documenting approach,
// so here we are providing some hand-written explanation about the most critical endpoints.
type SmugMug struct {
	*metadata.Base
	Response rootResponse `json:"response"`
}

func (e *SmugMug) endpointType() EndpointType {
	return EndpointSmugMug
}

func (e *SmugMug) GetAuthUser(c client) (*User, error) {
	return e.Response.Uris.AuthUser.Get(c)
}

type rootResponse struct {
	*metadata.Response
	Uris rootUris `json:"uris"`
}

type rootUris struct {
	// Currently Authenticated user.
	AuthUser authUserURI `json:"authUser"`
	// Base URI for user-related operations.
	UserBase userBaseURI `json:"userBase"`
	// Base URI for album-related operations.
	AlbumBase albumBaseURI `json:"albumBase"`
	// Base URI for album template-related operations.
	AlbumTemplateBase albumTemplateBaseURI `json:"albumTemplateBase"`
	// Base URI for theme-related operations.
	ThemeBase themeBaseURI `json:"themeBase"`
	// Base URI for template-related operations.
	TemplateBase templateBaseURI `json:"templateBase"`
	// Base URI for image-related operations.
	ImageBase imageBaseURI `json:"imageBase"`
	// Base URI for watermark-related operations.
	WatermarkBase watermarkBaseURI `json:"watermarkBase"`
	// Base URI for printmark-related operations.
	PrintmarkBase printmarkBaseURI `json:"printmarkBase"`
	// Base URI for folder-related operations.
	FolderBase folderBaseURI `json:"folderBase"`
	// Base URI for comment-related operations.
	CommentBase commentBaseURI `json:"commentBase"`
	// Base URI for page-related operations.
	PageBase pageBaseURI `json:"pageBase"`
	// Base URI for user status-related operations.
	StatusBase statusBaseURI `json:"statusBase"`
	// Base URI for download-related operations.
	DownloadBase downloadBaseURI `json:"downloadBase"`
	// Lookup folder, album, or page by user nickname and path.
	NicknameURLPathLookup nicknameURLPathLookupURI `json:"nicknameUrlPathLookup"`
	// Lookup folder, album, image (with an album context) or page by WebUri.
	WebURILookup webURILookupURI `json:"webUriLookup"`
	// Base URI for guide-related operations.
	GuideBase guideBaseURI `json:"guideBase"`
}
