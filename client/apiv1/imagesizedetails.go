package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeImageSizes returns a decoded SmugMug image size details response.
func DecodeImageSizeDetails(clt client, data []byte) (*ImageSizeDetails, error) {
	endpoint := new(ImageSizeDetails)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetImageSizes will return the image size details at the specified URI.
func GetImageSizeDetails(clt client, uri string, apiConfig *APIConfig) (*ImageSizeDetails, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [ImageSizeDetails] resource: %w", err)
	}

	return DecodeImageSizeDetails(clt, resourceData)
}

type ImageSizeDetails struct {
	*metadata.Base
	Response imageSizeDetailsResponse `json:"response,omitempty"`
}

func (e *ImageSizeDetails) endpointType() EndpointType {
	return EndpointImageSizeDetails
}

type imageSizeDetailsResponse struct {
	*metadata.Response
	Details imageSizeDetails `json:"imageSizeDetails,omitempty"`
}

type imageSizeDetails struct {
	ImageSizeX5Large  imageSizeDetail     `json:"imageSizeX5Large"`
	ImageSizeX4Large  imageSizeDetail     `json:"imageSizeX4Large"`
	ImageSizeX3Large  imageSizeDetail     `json:"imageSizeX3Large"`
	ImageSizeX2Large  imageSizeDetail     `json:"imageSizeX2Large"`
	ImageSizeXLarge   imageSizeDetail     `json:"imageSizeXLarge"`
	ImageSizeLarge    imageSizeDetail     `json:"imageSizeLarge"`
	ImageSizeMedium   imageSizeDetail     `json:"imageSizeMedium"`
	ImageSizeOriginal imageSizeDetail     `json:"imageSizeOriginal"`
	ImageSizeSmall    imageSizeDetail     `json:"imageSizeSmall"`
	ImageSizeThumb    imageSizeDetail     `json:"imageSizeThumb"`
	ImageSizeTiny     imageSizeDetail     `json:"imageSizeTiny"`
	ImageURLTemplate  JSONURL             `json:"imageUrlTemplate"`
	LargestImageSize  string              `json:"largestImageSize"`
	URI               string              `json:"uri"`
	URIDescription    string              `json:"uriDescription"`
	Uris              imageSizeDetailURIs `json:"uris"`
	UsableSizes       []string            `json:"usableSizes"`
}

type imageSizeDetail struct {
	Ext         string  `json:"ext"`
	Height      int     `json:"height"`
	Size        int     `json:"size"`
	URL         JSONURL `json:"url"`
	Watermarked bool    `json:"watermarked"`
	Width       int     `json:"width"`
	OwnerOnly   bool    `json:"ownerOnly"`
}

type imageSizeDetailURIs struct {
	ImageSizeCustom imageSizeCustom `json:"imageSizeCustom"`
}

type imageSizeCustom struct {
	EndpointType   string `json:"endpointType"`
	Locator        string `json:"locator"`
	LocatorType    string `json:"locatorType"`
	URI            string `json:"uri"`
	URIDescription string `json:"uriDescription"`
}
