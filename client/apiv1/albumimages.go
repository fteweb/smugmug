package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeAlbumImages returns an initialized SmugMug album images configuration.
func DecodeAlbumImages(clt client, data []byte) (*AlbumImages, error) {
	endpoint := new(AlbumImages)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetAlbumImages will return the album images at the specified URI.
func GetAlbumImages(clt client, uri string, apiConfig *APIConfig) (*AlbumImages, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [AlbumImages] resource: %w", err)
	}

	return DecodeAlbumImages(clt, resourceData)
}

type AlbumImages struct {
	*metadata.Base
	Response albumImageResponse `json:"response,omitempty"`
}

func (e *AlbumImages) endpointType() EndpointType {
	return EndpointAlbumImages
}

func (e *AlbumImages) GetImage(index int) (*ImageDetails, error) {
	if len(e.Response.Details) >= index {
		return e.Response.Details[index], nil
	}

	return nil, ErrInvalidChildNode
}

type albumImageResponse struct {
	*metadata.Response
	Details []*ImageDetails `json:"albumImage,omitempty"`
}
