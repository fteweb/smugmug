package apiv1

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

func NewUpload(albumURI string) *UploadConfig {
	uc := new(UploadConfig)
	uc.Version = "v2"
	uc.ResponseType = "JSON"
	uc.AlbumURI = albumURI

	return uc
}

type UploadConfig struct {
	AlbumURI     string
	Altitude     string
	Caption      string
	FileName     string
	Hidden       bool
	ImageURI     string
	Keywords     []string
	Latitude     string
	Longitude    string
	Pretty       bool
	ResponseType string
	Title        string
	Version      string
}

func (uc *UploadConfig) SetHeaders(req *http.Request) { //nolint:cyclop
	// Set required headers
	u, err := url.Parse(uc.AlbumURI)
	if err != nil {
		log.Println("Error parsing AlbumURI for upload header")
	}

	req.Header.Set("X-Smug-AlbumUri", u.Path)
	req.Header.Set("X-Smug-ResponseType", uc.ResponseType)
	req.Header.Set("X-Smug-Version", uc.Version)

	// Set optional headers
	if uc.Altitude != "" {
		req.Header.Set("X-Smug-Altitude", uc.Altitude)
	}

	if uc.Caption != "" {
		req.Header.Set("X-Smug-Caption", uc.Caption)
	}

	if uc.FileName != "" {
		req.Header.Set("X-Smug-FileName", uc.FileName)
	}

	if uc.Hidden {
		req.Header.Set("X-Smug-Hidden", "true")
	} else {
		req.Header.Set("X-Smug-Hidden", "false")
	}

	if uc.ImageURI != "" {
		req.Header.Set("X-Smug-ImageUri", uc.ImageURI)
	}

	if len(uc.Keywords) != 0 {
		req.Header.Set("X-Smug-Keywords", strings.Join(uc.Keywords, ","))
	}

	if uc.Latitude != "" {
		req.Header.Set("X-Smug-Latitude", uc.Latitude)
	}

	if uc.Longitude != "" {
		req.Header.Set("X-Smug-Longitude", uc.Longitude)
	}

	if uc.Pretty {
		req.Header.Set("X-Smug-Pretty", "true")
	} else {
		req.Header.Set("X-Smug-Pretty", "false")
	}

	if uc.Title != "" {
		req.Header.Set("X-Smug-Title", uc.Title)
	}
}

func (uc *UploadConfig) Verify() bool {
	if uc.AlbumURI == "" {
		log.Println("Upload Config is missing the destination Album URI")

		return false
	}

	if uc.ResponseType == "" {
		log.Println("Upload Config is missing the response type (JSON/PHP)")

		return false
	}

	if uc.Version == "" {
		log.Println("Upload Config is missing the API version (v2)")

		return false
	}

	return true
}

func (uc *UploadConfig) GetFilename() string {
	return uc.FileName
}

func NewUploadResponse(data []byte) (*UploadResponse, error) {
	endpoint := new(UploadResponse)
	dec := json.NewDecoder(bytes.NewReader(data))
	dec.DisallowUnknownFields() // Force errors

	if err := dec.Decode(&endpoint); err != nil {
		return nil, fmt.Errorf("failed to decode upload response: %w", err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode upload response: %w", err)
	}

	return endpoint, nil
}

type UploadResponse struct {
	Stat   string              `json:"stat"`
	Method string              `json:"method"`
	Image  uploadImageResponse `json:"image"`
	Asset  uploadImageAsset    `json:"asset"`
	*metadata.Status
}

type uploadImageResponse struct {
	ImageURI              string `json:"imageUri"`
	AlbumImageURI         string `json:"albumImageUri"`
	StatusImageReplaceURI string `json:"statusImageReplaceUri"`
	URL                   string `json:"url"`
}

type uploadImageAsset struct {
	AssetComponentURI string `json:"assetComponentUri"`
	AssetURI          string `json:"assetUri"`
}
