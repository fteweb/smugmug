package apiv1

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeImage returns a decoded smugmug image response.
func DecodeImage(clt client, data []byte) (*Image, error) {
	endpoint := new(Image)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetImage will return the image at the specified URI.
func GetImage(clt client, uri string, apiConfig *APIConfig) (*Image, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [Image] resource: %w", err)
	}

	return DecodeImage(clt, resourceData)
}

type Image struct {
	*metadata.Base
	Response imageResponse `json:"response,omitempty"`
}

func (e *Image) endpointType() EndpointType {
	return EndpointImage
}

func (e *Image) GetSizes(c client) (*ImageSizeDetails, error) {
	return e.Response.Details.GetSizes(c)
}

type imageResponse struct {
	*metadata.Response
	Details ImageDetails `json:"image,omitempty"`
}

// ImageDetails is the actual endpoint.
type ImageDetails struct {
	Title                         string             `json:"title"`
	Caption                       string             `json:"caption"`
	Keywords                      string             `json:"keywords"`
	KeywordArray                  []string           `json:"keywordArray"`
	Watermark                     string             `json:"watermark"`
	Latitude                      string             `json:"latitude"`
	Longitude                     string             `json:"longitude"`
	Altitude                      int                `json:"altitude"`
	Hidden                        bool               `json:"hidden"`
	ThumbnailURL                  *JSONURL           `json:"thumbnailUrl"`
	FileName                      string             `json:"fileName"`
	Processing                    bool               `json:"processing"`
	UploadKey                     string             `json:"uploadKey"`
	DateTimeUploaded              time.Time          `json:"dateTimeUploaded"`
	Date                          time.Time          `json:"date"`
	Format                        string             `json:"format"`
	OriginalHeight                uint               `json:"originalHeight"`
	OriginalWidth                 uint               `json:"originalWidth"`
	OriginalSize                  uint               `json:"originalSize"`
	LastUpdated                   time.Time          `json:"lastUpdated"`
	Collectable                   bool               `json:"collectable"`
	IsArchive                     bool               `json:"isArchive"`
	IsVideo                       bool               `json:"isVideo"`
	ComponentFileTypes            componentFileTypes `json:"componentFileTypes"`
	CanEdit                       bool               `json:"canEdit"`
	CanBuy                        bool               `json:"canBuy"`
	Protected                     bool               `json:"protected"`
	Watermarked                   bool               `json:"watermarked"`
	ImageKey                      string             `json:"imageKey"`
	Serial                        int                `json:"serial"`
	ArchivedURI                   *JSONURL           `json:"archivedUri"`
	ArchivedSize                  int                `json:"archivedSize"`
	ArchivedMD5                   string             `json:"archivedMd5"`
	CanShare                      bool               `json:"canShare"`
	Comments                      bool               `json:"comments"`
	ShowKeywords                  bool               `json:"showKeywords"`
	FormattedValues               formattedValues    `json:"formattedValues"`
	PreferredDisplayFileExtension string             `json:"preferredDisplayFileExtension"`
	URI                           string             `json:"uri"`
	WebURI                        *JSONURL           `json:"webUri"`
	URIDescription                string             `json:"uriDescription"`
	Uris                          imageUris          `json:"uris"`
	Movable                       bool               `json:"movable"`
	Origin                        string             `json:"origin"`
}

// GetAlbum returns the album ID for the image.
func (e *ImageDetails) GetAlbum() string {
	return strings.Split(e.URI, "/")[4]
}

// GetSizes fetched the image sizes from the API.
func (e *ImageDetails) GetSizes(c client) (*ImageSizeDetails, error) {
	return e.Uris.ImageSizeDetails.Get(c)
}

type componentFileTypes struct {
	Image []string `json:"image"`
}

type formattedValues struct {
	Caption  imageCaption  `json:"caption"`
	FileName imageFileName `json:"fileName"`
}
type imageCaption struct {
	HTML string `json:"html"`
	Text string `json:"text"`
}
type imageFileName struct {
	HTML string `json:"html"`
	Text string `json:"text"`
}
type imageUris struct {
	LargestImage                  largestImageURI                  `json:"largestImage"`
	ImageSizes                    imageSizesURI                    `json:"imageSizes"`
	ImageSizeDetails              imageSizeDetailsURI              `json:"imageSizeDetails"`
	PointOfInterest               pointOfInterestURI               `json:"pointOfInterest"`
	PointOfInterestCrops          pointOfInterestCropsURI          `json:"pointOfInterestCrops"`
	Regions                       regionsURI                       `json:"regions"`
	ImageAlbum                    imageAlbumURI                    `json:"imageAlbum"`
	ImageComments                 imageCommentsURI                 `json:"imageComments"`
	ImageDownload                 imageDownloadURI                 `json:"imageDownload"`
	ImageOwner                    imageOwnerURI                    `json:"imageOwner"`
	ImageAlbums                   imageAlbumsURI                   `json:"imageAlbums"`
	RotateImage                   rotateImageURI                   `json:"rotateImage"`
	ColorImage                    colorImageURI                    `json:"colorImage"`
	CopyImage                     copyImageURI                     `json:"copyImage"`
	CropImage                     cropImageURI                     `json:"cropImage"`
	ImageMetadata                 imageMetadataURI                 `json:"imageMetadata"`
	ImagePrices                   imagePricesURI                   `json:"imagePrices"`
	ImagePricelistExclusions      imagePricelistExclusionsURI      `json:"imagePricelistExclusions"`
	Image                         imageURI                         `json:"image"`
	Album                         albumURI                         `json:"album"`
	AlbumImagePricelistExclusions albumImagePricelistExclusionsURI `json:"albumImagePricelistExclusions"`
	AlbumImageMetadata            albumImageMetadataURI            `json:"albumImageMetadata"`
	AlbumImageShareUris           albumImageShareUrisURI           `json:"albumImageShareUris"`
}
