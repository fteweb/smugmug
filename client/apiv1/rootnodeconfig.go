package apiv1

// NewRootNode returns an initialized SmugMug root node configuration.
func NewRootNode() *RootNodeConfig {
	node := new(RootNodeConfig)

	return node
}

type RootNodeConfig struct {
	nodeConfig
}

func (ac *RootNodeConfig) SetSmugSearchableNo() {
	ac.smugSearchable = searchableNo
}

func (ac *RootNodeConfig) SetSmugSearchableInherit() {
	ac.smugSearchable = searchableInherit
}

func (ac *RootNodeConfig) SetWorldSearchableNo() {
	ac.worldSearchable = searchableNo
}

func (ac *RootNodeConfig) SetWorldSearchableInherit() {
	ac.worldSearchable = searchableInherit
}

func (ac *RootNodeConfig) Verify() error {
	return ac.nodeConfig.Verify()
}
