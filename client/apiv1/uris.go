package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

type authUserURI struct {
	*metadata.Resource
	Details *userDetails `json:"user,omitempty"`
}

func (u *authUserURI) Get(c client) (*User, error) {
	return GetUser(c, u.URI, nil)
}

type userBaseURI struct {
	*metadata.Resource
}
type nodeURI struct {
	*metadata.Resource
	Details *NodeDetails `json:"node,omitempty"`
}

func (u *nodeURI) Get(c client) (*Node, error) {
	return GetNode(c, u.URI, nil)
}

type albumBaseURI struct {
	*metadata.Resource
}
type albumTemplateBaseURI struct {
	*metadata.Resource
}
type themeBaseURI struct {
	*metadata.Resource
}
type templateBaseURI struct {
	*metadata.Resource
}
type imageBaseURI struct {
	*metadata.Resource
}
type watermarkBaseURI struct {
	*metadata.Resource
}
type printmarkBaseURI struct {
	*metadata.Resource
}
type folderBaseURI struct {
	*metadata.Resource
}
type commentBaseURI struct {
	*metadata.Resource
}
type pageBaseURI struct {
	*metadata.Resource
}
type statusBaseURI struct {
	*metadata.Resource
}
type downloadBaseURI struct {
	*metadata.Resource
}
type nicknameURLPathLookupURI struct {
	*metadata.Resource
}
type webURILookupURI struct {
	*metadata.Resource
}
type guideBaseURI struct {
	*metadata.Resource
}
type subUserURI struct {
	*metadata.Resource
}

// TODO: Do somthing with this URI.
type userSearchURI struct { //nolint:deadcode,unused
	*metadata.Resource
}
type folderByIDURI struct {
	*metadata.Resource
}
type parentNodesURI struct {
	*metadata.Resource
}
type nodeCoverImageURI struct {
	*metadata.Resource
}
type highlightImageURI struct {
	*metadata.Resource
}
type nodeCommentsURI struct {
	*metadata.Resource
}
type childNodesURI struct {
	*metadata.Resource
	Details []*NodeDetails `json:"node,omitempty"`
}

func (u *childNodesURI) Get(c client) (*ChildNode, error) {
	return GetChildNode(c, u.URI, nil)
}

var ErrPagerNotFound = fmt.Errorf("page option does not exist")

func (u *childNodesURI) GetFirstPage(c client) (*ChildNode, error) {
	if u.Pages.FirstPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.FirstPage, nil)
}

func (u *childNodesURI) GetLastPage(c client) (*ChildNode, error) {
	if u.Pages.LastPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.LastPage, nil)
}

func (u *childNodesURI) GetPrevPage(c client) (*ChildNode, error) {
	if u.Pages.PrevPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.PrevPage, nil)
}

func (u *childNodesURI) GetNextPage(c client) (*ChildNode, error) {
	if u.Pages.NextPage == "" {
		return nil, ErrPagerNotFound
	}

	return GetChildNode(c, u.Pages.NextPage, nil)
}

type moveNodesURI struct {
	*metadata.Resource
}
type bioImageURI struct {
	*metadata.Resource
}
type coverImageURI struct {
	*metadata.Resource
}
type userProfileURI struct {
	*metadata.Resource
}
type folderURI struct {
	*metadata.Resource
}
type featuresURI struct {
	*metadata.Resource
}
type siteSettingsURI struct {
	*metadata.Resource
}
type userAlbumsURI struct {
	*metadata.Resource
	Details []*AlbumDetails `json:"album,omitempty"`
}
type userGeoMediaURI struct {
	*metadata.Resource
}
type userPopularMediaURI struct {
	*metadata.Resource
}
type userFeaturedAlbumsURI struct {
	*metadata.Resource
}
type userRecentImagesURI struct {
	*metadata.Resource
}
type userImageSearchURI struct {
	*metadata.Resource
}
type userTopKeywordsURI struct {
	*metadata.Resource
}
type urlPathLookupURI struct {
	*metadata.Resource
}
type userAlbumTemplatesURI struct {
	*metadata.Resource
}
type sortUserFeaturedAlbumsURI struct {
	*metadata.Resource
}
type userTasksURI struct {
	*metadata.Resource
}
type userWatermarksURI struct {
	*metadata.Resource
}
type userPrintmarksURI struct {
	*metadata.Resource
}
type userUploadLimitsURI struct {
	*metadata.Resource
}
type userAssetsAlbumURI struct {
	*metadata.Resource
}
type userLatestQuickNewsURI struct {
	*metadata.Resource
}
type userGuideStatesURI struct {
	*metadata.Resource
}
type userHideGuidesURI struct {
	*metadata.Resource
}
type userGrantsURI struct {
	*metadata.Resource
}
type duplicateImageSearchURI struct {
	*metadata.Resource
}
type userDeletedAlbumsURI struct {
	*metadata.Resource
}
type userDeletedFoldersURI struct {
	*metadata.Resource
}
type userDeletedPagesURI struct {
	*metadata.Resource
}
type userContactsURI struct {
	*metadata.Resource
}
type rawManagementAddOnStatusURI struct {
	*metadata.Resource
}
type albumShareUrisURI struct {
	*metadata.Resource
}
type parentFoldersURI struct {
	*metadata.Resource
}
type albumHighlightImageURI struct {
	*metadata.Resource
}
type albumImagesURI struct {
	*metadata.Resource
	Details []*ImageDetails `json:"albumImage,omitempty"`
}

func (u *albumImagesURI) Get(c client) (*AlbumImages, error) {
	return GetAlbumImages(c, u.URI, nil)
}

type albumPopularMediaURI struct {
	*metadata.Resource
}
type albumGeoMediaURI struct {
	*metadata.Resource
}
type albumCommentsURI struct {
	*metadata.Resource
}
type moveAlbumImagesURI struct {
	*metadata.Resource
}
type collectImagesURI struct {
	*metadata.Resource
}
type applyAlbumTemplateURI struct {
	*metadata.Resource
}
type deleteAlbumImagesURI struct {
	*metadata.Resource
}
type uploadFromExternalResourceURI struct {
	*metadata.Resource
}
type uploadFromURI struct {
	*metadata.Resource
}
type albumDownloadURI struct {
	*metadata.Resource
}
type albumPricesURI struct {
	*metadata.Resource
}
type albumPricelistExclusionsURI struct {
	*metadata.Resource
}
type albumURI struct {
	*metadata.Resource
	Details *AlbumDetails `json:"album,omitempty"`
}

func (u *albumURI) Get(c client) (*Album, error) {
	return GetAlbum(c, u.URI, nil)
}

type parentNodeURI struct {
	*metadata.Resource
}
type imageSizesURI struct {
	*metadata.Resource
	Details *imageSizesDetails `json:"imageSizes,omitempty"`
}

func (u *imageSizesURI) Get(c client) (*ImageSizes, error) {
	return GetImageSizes(c, u.URI, nil)
}

type imageSizeDetailsURI struct {
	*metadata.Resource
	Details *imageSizeDetails `json:"imageSizeDetails,omitempty"`
}

func (u *imageSizeDetailsURI) Get(c client) (*ImageSizeDetails, error) {
	return GetImageSizeDetails(c, u.URI, nil)
}

type pointOfInterestURI struct {
	*metadata.Resource
}
type pointOfInterestCropsURI struct {
	*metadata.Resource
}
type regionsURI struct {
	*metadata.Resource
}
type imageAlbumURI struct {
	*metadata.Resource
}
type imageDownloadURI struct {
	*metadata.Resource
}
type imageOwnerURI struct {
	*metadata.Resource
}
type imageAlbumsURI struct {
	*metadata.Resource
}
type rotateImageURI struct {
	*metadata.Resource
}
type colorImageURI struct {
	*metadata.Resource
}
type copyImageURI struct {
	*metadata.Resource
}
type cropImageURI struct {
	*metadata.Resource
}
type imageMetadataURI struct {
	*metadata.Resource
}
type imagePricesURI struct {
	*metadata.Resource
}
type imagePricelistExclusionsURI struct {
	*metadata.Resource
}
type imageURI struct {
	*metadata.Resource
}
type albumImagePricelistExclusionsURI struct {
	*metadata.Resource
}
type albumImageMetadataURI struct {
	*metadata.Resource
}
type albumImageShareUrisURI struct {
	*metadata.Resource
}
type imageSizeTinyURI struct {
	*metadata.Resource
}
type imageSizeThumbURI struct {
	*metadata.Resource
}
type imageSizeSmallURI struct {
	*metadata.Resource
}
type imageSizeMediumURI struct {
	*metadata.Resource
}
type imageSizeLargeURI struct {
	*metadata.Resource
}
type imageSizeXLargeURI struct {
	*metadata.Resource
}
type imageSizeX2LargeURI struct {
	*metadata.Resource
}
type imageSizeX3LargeURI struct {
	*metadata.Resource
}
type imageSizeOriginalURI struct {
	*metadata.Resource
}
type imageSizeCustomURI struct {
	*metadata.Resource
}
type largestImageURI struct {
	*metadata.Resource
}
type sortAlbumImagesURI struct {
	*metadata.Resource
}
type imageCommentsURI struct {
	*metadata.Resource
}
