package apiv1

import (
	"fmt"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeImageSizes returns a decoded SmugMug image size response.
func DecodeImageSizes(clt client, data []byte) (*ImageSizes, error) {
	endpoint := new(ImageSizes)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetImageSizes will return the image sizes at the specified URI.
func GetImageSizes(clt client, uri string, apiConfig *APIConfig) (*ImageSizes, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [ImageSizes] resource: %w", err)
	}

	return DecodeImageSizes(clt, resourceData)
}

type ImageSizes struct {
	*metadata.Base
	Response imageSizesResponse `json:"response,omitempty"`
}

func (e *ImageSizes) endpointType() EndpointType {
	return EndpointImageSizes
}

type imageSizesResponse struct {
	*metadata.Response
	Details imageSizesDetails `json:"imageSizes,omitempty"`
}
type imageSizesDetails struct {
	TinyImageURL     *JSONURL       `json:"tinyImageUrl"`
	ThumbImageURL    *JSONURL       `json:"thumbImageUrl"`
	SmallImageURL    *JSONURL       `json:"smallImageUrl"`
	MediumImageURL   *JSONURL       `json:"mediumImageUrl"`
	LargeImageURL    *JSONURL       `json:"largeImageUrl"`
	XLargeImageURL   *JSONURL       `json:"xLargeImageUrl"`
	X2LargeImageURL  *JSONURL       `json:"x2LargeImageUrl"`
	X3LargeImageURL  *JSONURL       `json:"x3LargeImageUrl"`
	OriginalImageURL *JSONURL       `json:"originalImageUrl"`
	LargestImageURL  *JSONURL       `json:"largestImageUrl"`
	URI              string         `json:"uri"`
	URIDescription   string         `json:"uriDescription"`
	Uris             imageSizesUris `json:"uris"`
}
type imageSizesUris struct {
	ImageSizeTiny     imageSizeTinyURI     `json:"imageSizeTiny"`
	ImageSizeThumb    imageSizeThumbURI    `json:"imageSizeThumb"`
	ImageSizeSmall    imageSizeSmallURI    `json:"imageSizeSmall"`
	ImageSizeMedium   imageSizeMediumURI   `json:"imageSizeMedium"`
	ImageSizeLarge    imageSizeLargeURI    `json:"imageSizeLarge"`
	ImageSizeXLarge   imageSizeXLargeURI   `json:"imageSizeXLarge"`
	ImageSizeX2Large  imageSizeX2LargeURI  `json:"imageSizeX2Large"`
	ImageSizeX3Large  imageSizeX3LargeURI  `json:"imageSizeX3Large"`
	ImageSizeOriginal imageSizeOriginalURI `json:"imageSizeOriginal"`
	ImageSizeCustom   imageSizeCustomURI   `json:"imageSizeCustom"`
	LargestImage      largestImageURI      `json:"largestImage"`
}
