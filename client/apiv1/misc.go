package apiv1

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"strings"
)

type ItemConfig interface {
	Verify() error
	Create() ([]byte, error)
}

type client interface {
	DebugState() bool
	Errorf(string, ...interface{}) error
	GetResource(string, *APIConfig) ([]byte, error)
	UploadResource(io.Reader, *UploadConfig) ([]byte, error)
	PostResource(string, *APIConfig, ItemConfig) ([]byte, error)
	Basepath(string) string
	GenerateResponsePath() string
}

type apiEndpoint interface {
	endpointType() EndpointType
}

func decodeResponse(clt client, endpoint apiEndpoint, data []byte) error {
	if clt.DebugState() {
		// if err := os.WriteFile(c.GenerateResponsePath(), data, os.FileMode(0644)); err != nil {
		// 	return c.Errorf("failed to write response debug file: %w", err)
		// }
		dec := json.NewDecoder(bytes.NewReader(data))
		dec.DisallowUnknownFields() // Force errors

		if err := dec.Decode(&endpoint); err != nil {
			return fmt.Errorf(
				"failed to decode JSON response into the selected endpoint [%s]: %w",
				endpoint.endpointType(),
				err,
			)
		}

		return nil
	}

	if err := json.Unmarshal(data, endpoint); err != nil {
		return fmt.Errorf(
			"failed to unmarshal JSON response into the selected endpoint [%s]: %w",
			endpoint.endpointType(),
			err,
		)
	}

	return nil
}

type JSONURL url.URL

func (ju *JSONURL) UnmarshalJSON(j []byte) error {
	// log.Printf("JSON URL: %q", strings.ReplaceAll(string(j), "\"", ""))
	u, err := url.Parse(strings.ReplaceAll(string(j), "\"", ""))
	if err != nil {
		return fmt.Errorf("failed to unmarshal JSON URL: %w", err)
	}

	*ju = JSONURL(*u)

	return nil
}

type EndpointType string

const (
	EndpointAlbum            EndpointType = "Album"
	EndpointAlbumImages      EndpointType = "AlbumImages"
	EndpointChildNode        EndpointType = "ChildNode"
	EndpointImage            EndpointType = "Image"
	EndpointImageSizeDetails EndpointType = "ImageSizeDetails"
	EndpointImageSizes       EndpointType = "ImageSizes"
	EndpointNode             EndpointType = "Node"
	EndpointSmugMug          EndpointType = "RootNode"
	EndpointUser             EndpointType = "User"
)
