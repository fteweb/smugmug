package apiv1

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/dghubble/oauth1"
	"gitlab.com/fteweb/smugmug/client/metadata"
)

// Root domain for Token URIs.
const (
	// OAuth root domain.
	OAuthDomain = "https://secure.smugmug.com"
	// Endpoint to request a temporary token.
	OAuthRequestTokenEndpoint = "/services/oauth/1.0a/getRequestToken" //nolint:gosec
	// Endpoint to authorize application access.
	OAuthAuthorizeEndpoint = "/services/oauth/1.0a/authorize"
	// Endpoint to generate an Access Token.
	OAuthAccessTokenEndpoint = "/services/oauth/1.0a/getAccessToken" //nolint:gosec
	// API version.
	APIVersion = "v2"
	// API Domain.
	APIDomain = "https://api.smugmug.com"
	// Upload Domain.
	UploadDomain = "https://upload.smugmug.com"
)

func NewClientOptions(apiKey string, apiSecret string) ClientOptions {
	return ClientOptions{
		RateLimit: new(rateLimit),
		Oauth: OauthOptions{
			Config: &oauth1.Config{
				ConsumerKey:    apiKey,
				ConsumerSecret: apiSecret,
				CallbackURL:    "oob", // Default for non-web authorization, SmugMug will provide a 6 digit code to authorize.
				Endpoint: oauth1.Endpoint{
					RequestTokenURL: OAuthDomain + OAuthRequestTokenEndpoint,
					AuthorizeURL:    OAuthDomain + OAuthAuthorizeEndpoint + "?Access=Full&Permissions=Modify",
					AccessTokenURL:  OAuthDomain + OAuthAccessTokenEndpoint,
				},
				Realm:      "",
				Signer:     nil,
				Noncer:     nil,
				HTTPClient: nil,
			},
			Domain: OAuthDomain,
			Token:  new(oauth1.Token),
		},
		API: APIOptions{
			Version:      APIVersion,
			Domain:       APIDomain,
			UploadDomain: UploadDomain,
		},
		Verbosity:       0,
		Pretty:          false,
		InlineExpansion: false,
		Expand:          nil,
		Timing:          false,
	}
}

// ClientOptions is the client configuration options
// Use the NewClientOptions() function to instantiate unless you know what you are doing!
type ClientOptions struct {
	// API Verbosity
	//
	// _verbosity=1
	// No Options section.
	// This also implies _shorturis=, and removes a few metadata fields that appear in the Response section.
	//
	// _verbosity=2
	// If the current request method has parameters, there will be an Options section listing those parameters.
	// The parameters of other request methods will not be included.
	// This is the default verbosity level, except for OPTIONS requests.
	//
	// _verbosity=3
	// An Options section will be included, and parameters will be listed for all request methods.
	// This is the default verbosity level for OPTIONS requests.
	Verbosity int `json:"-"`
	// Response is formatted JSON rather than a no whitespace string.
	Pretty bool `json:"-"`
	// Expand URIs inline.
	InlineExpansion bool `json:"-"`
	// Expand URIs.
	Expand []string `json:"-"`
	// Timing metrics.
	Timing bool `json:"-"`
	// Rate Limit struct.
	RateLimit *rateLimit
	// Smugmug API Oauth Information.
	Oauth OauthOptions `json:"-"`
	// Smugmug API Information.
	API APIOptions `json:"-"`
}

// SetAccessToken will save the given Access token and secret.
func (c *ClientOptions) SetAccessToken(token string, secret string) {
	c.Oauth.Token = &oauth1.Token{
		Token:       token,
		TokenSecret: secret,
	}
}

// GetAPIKey will return the configured API Key and Secret.
func (c *ClientOptions) GetAPIKey() (string, string) {
	return c.Oauth.Config.ConsumerKey, c.Oauth.Config.ConsumerSecret
}

// AppendExpansion to current list.
func (c *ClientOptions) AppendExpansion(expand ...string) {
	c.Expand = append(c.Expand, expand...)
}

// NewExpansion overwrites previous expansions.
func (c *ClientOptions) NewExpansion(expand ...string) {
	c.Expand = expand
}

// ClearExpansion deletes the expansion config.
func (c *ClientOptions) ClearExpansion() {
	c.Expand = []string{}
}

func (c *ClientOptions) Query(method string, endpoint string, config *APIConfig, body []byte) (*http.Request, error) {
	req, err := http.NewRequest(method, c.API.Domain+endpoint, bytes.NewBuffer(body))
	if err != nil {
		return nil, fmt.Errorf("failed to build API request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")

	query := req.URL.Query() // Get a copy of the query values.
	if c.Verbosity != 0 {
		query.Add("_verbosity", fmt.Sprintf("%d", c.Verbosity))
	}

	if c.Pretty {
		query.Add("_pretty", "")
	}

	if c.InlineExpansion {
		query.Add("_expandmethod", "inline")
	}

	if c.Timing {
		query.Add("_timing", "")
	}

	if len(c.Expand) != 0 {
		query.Add("_expand", strings.Join(c.Expand, ","))
	}

	if config != nil && config.Enabled() {
		configHex, err := json.Marshal(config)
		if err != nil {
			return nil, fmt.Errorf("failed to JSON Marshal config parameter for API query: %w", err)
		}
		// configString := string(configHex)
		// fmt.Println(configString)
		query.Add("_config", string(configHex))
	}

	req.URL.RawQuery = query.Encode() // Encode and assign back to the original query.

	return req, nil
}

type OauthOptions struct {
	Config *oauth1.Config
	Domain string
	Token  *oauth1.Token
}

type APIOptions struct {
	Version      string
	Domain       string
	UploadDomain string
}

func (a APIOptions) BasePath() string {
	return "/api/" + a.Version + "/"
}

// SmugMug API Rate Limiting.
type rateLimit struct {
	Remaining  int
	Reset      time.Time
	RetryAfter time.Duration
}

var ErrAPIRateLimited = fmt.Errorf("no API calls available")

// Available checks if we have API requests available.
func (r *rateLimit) Available() error {
	if r.Reset.After(time.Now()) && r.Remaining == 0 {
		return fmt.Errorf("%w, please retry after %v", ErrAPIRateLimited, r.Reset)
	}

	return nil
}

var ErrAPILimitExceeded = fmt.Errorf("API call limit exceeded")

const (
	parseIntBase     = 10
	parseIntBaseSize = 64
)

func (r *rateLimit) Decrement(uploadDomain string, req *http.Request, response *http.Response) error {
	// Uploads do not count towards API limit.
	if strings.Contains(uploadDomain, req.URL.Host) {
		return nil
	}

	var err error

	// Save the remaining amount of API calls.
	// log.Println("Header Remaining: " + response.Header.Get("X-RateLimit-Remaining"))
	r.Remaining, err = strconv.Atoi(response.Header.Get("X-RateLimit-Remaining"))
	if err != nil {
		return fmt.Errorf("failed to extract [X-RateLimit-Remaining] from API response: %w", err)
	}

	// Save the next API call reset.
	// log.Println("Header Reset: " + response.Header.Get("X-RateLimit-Reset"))
	i, err := strconv.ParseInt(response.Header.Get("X-RateLimit-Reset"), parseIntBase, parseIntBaseSize)
	if err != nil {
		return fmt.Errorf("failed to parse [X-RateLimit-Reset] to unix time: %w", err)
	}

	r.Reset = time.Unix(i, 0)

	// No API calls remaining.
	if response.StatusCode == metadata.ErrCodeTooManyRequests {
		r.RetryAfter, err = time.ParseDuration(response.Header.Get("Retry-After") + "s")
		if err != nil {
			return fmt.Errorf("failed to parse [Retry-After] duration from API response: %w", err)
		}

		return fmt.Errorf("%w, try again after %gs", ErrAPILimitExceeded, r.RetryAfter.Seconds())
	}

	return nil
}
