package apiv1

func NewAPIConfig() *APIConfig {
	return new(APIConfig)
}

// NewExpansion returns an expansion configuration.
func NewExpansion(uri string) map[string]*APIConfig {
	newMap := make(map[string]*APIConfig)
	newMap[uri] = NewAPIConfig()

	return newMap
}

type APIConfig struct {
	Enable    bool                  `json:"-"`
	Filter    []string              `json:"filter,omitempty"`
	FilterURI []string              `json:"filteruri,omitempty"`
	Expand    map[string]*APIConfig `json:"expand,omitempty"`
	MultiArgs []string              `json:"multiargs,omitempty"`
}

func (c *APIConfig) IsAPIConfig() {
}

func (c *APIConfig) Enabled() bool {
	if c != NewAPIConfig() && c.Enable {
		return true
	}

	return false
}

// Add expansion to current list.
func (c *APIConfig) AppendExpansion(expand string) *APIConfig {
	c.Expand[expand] = NewAPIConfig()

	return c.Expand[expand]
}

// Overwrite expansion list.
func (c *APIConfig) NewExpansion(expand string) *APIConfig {
	c.ClearExpansion()
	c.Expand[expand] = NewAPIConfig()

	return c.Expand[expand]
}

// Clear the expansion list.
func (c *APIConfig) ClearExpansion() {
	c.Expand = map[string]*APIConfig{}
}

// Add expansion to current list.
func (c *APIConfig) AppendFilter(filter ...string) {
	c.Filter = append(c.Filter, filter...)
}

// Overwrite expansion list.
func (c *APIConfig) NewFilter(filter ...string) {
	c.Filter = filter
}

// Clear the expansion list.
func (c *APIConfig) ClearFilter() {
	c.Filter = []string{}
}

// Add expansion to current list.
func (c *APIConfig) AppendFilterURI(filter ...string) *APIConfig {
	c.FilterURI = append(c.FilterURI, filter...)

	return c
}

// Overwrite expansion list.
func (c *APIConfig) NewFilterURI(filter ...string) {
	c.FilterURI = filter
}

// Clear the expansion list.
func (c *APIConfig) ClearFilterURI() {
	c.FilterURI = []string{}
}
