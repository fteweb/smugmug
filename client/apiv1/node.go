package apiv1

import (
	"fmt"
	"time"

	"gitlab.com/fteweb/smugmug/client/metadata"
)

// DecodeNode returns a decoded SmugMug node response.
func DecodeNode(clt client, data []byte) (*Node, error) {
	endpoint := new(Node)
	if err := decodeResponse(clt, endpoint, data); err != nil {
		return nil, clt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	if err := endpoint.CheckErr(); err != nil {
		return nil, fmt.Errorf("failed to decode %s: %w", endpoint.endpointType(), err)
	}

	return endpoint, nil
}

// GetNode will return the node at the specified URI.
func GetNode(clt client, uri string, apiConfig *APIConfig) (*Node, error) {
	resourceData, err := clt.GetResource(uri, apiConfig)
	if err != nil {
		return nil, clt.Errorf("failed to get [Node] resource: %w", err)
	}

	return DecodeNode(clt, resourceData)
}

// GetNodeWithChildren will return the node and child nodes at the specified URI.
func GetNodeWithChildren(clt client, uri string) (*Node, error) {
	// Configure API request to populate the Node with Children
	apiConfig := NewAPIConfig()
	apiConfig.Enable = true
	filter := apiConfig.AppendFilterURI("ChildNodes", "Album")
	filter.NewExpansion("ChildNodes")
	filter.AppendExpansion("Album")

	return GetNode(clt, uri, apiConfig)
}

type Node struct {
	*metadata.Base
	Response nodeResponse `json:"response,omitempty"`
}

func (e *Node) endpointType() EndpointType {
	return EndpointNode
}

func (e *Node) GetChildren(c client) (*ChildNode, error) {
	return e.Response.Details.GetChildren(c)
}

func (e *Node) GetAlbum(c client) (*Album, error) {
	return e.Response.Details.GetAlbum(c)
}

func (e *Node) NewNode(c client) (*ChildNode, error) {
	return e.Response.Details.GetChildren(c)
}

func (e *Node) NewAlbum(clt client, name string) (*Album, error) {
	// Create a new Album Node
	responseData, err := clt.PostResource(e.Response.URI, NewAPIConfig(), NewAlbumConfig(name))
	if err != nil {
		return nil, clt.Errorf("failed to create [album] resource: %w", err)
	}

	newAlbumNode, err := DecodeNode(clt, responseData)
	if err != nil {
		return nil, clt.Errorf("failed to decode new [album] response: %w", err)
	}

	return newAlbumNode.GetAlbum(clt)
}

type nodeResponse struct {
	*metadata.Response
	Details NodeDetails `json:"node,omitempty"`
	// List of sub-resources
	// Uris nodeUris `json:"Uris"`
}

// NodeDetails is the actual endpoint.
type NodeDetails struct {
	Description           string                  `json:"description,omitempty"`
	HideOwner             bool                    `json:"hideOwner,omitempty"`
	Name                  string                  `json:"name,omitempty"`
	Keywords              []string                `json:"keywords,omitempty"`
	Password              string                  `json:"password,omitempty"`
	PasswordHint          string                  `json:"passwordHint,omitempty"`
	Privacy               nodePrivacyString       `json:"privacy,omitempty"`
	SecurityType          nodeSecurityTypeString  `json:"securityType,omitempty"`
	ShowCoverImage        bool                    `json:"showCoverImage,omitempty"`
	SmugSearchable        nodeSearchableString    `json:"smugSearchable,omitempty"`
	SortDirection         nodeSortDirectionString `json:"sortDirection,omitempty"`
	SortMethod            nodeSortMethodString    `json:"sortMethod,omitempty"`
	Type                  nodeTypeString          `json:"type,omitempty"`
	URLName               string                  `json:"urlName,omitempty"`
	WorldSearchable       nodeSearchableString    `json:"worldSearchable,omitempty"`
	DateAdded             time.Time               `json:"dateAdded,omitempty"`
	DateModified          time.Time               `json:"dateModified,omitempty"`
	EffectivePrivacy      nodePrivacyString       `json:"effectivePrivacy,omitempty"`
	EffectiveSecurityType nodeSecurityTypeString  `json:"effectiveSecurityType,omitempty"`
	FormattedValues       struct {
		Name struct {
			HTML string `json:"html,omitempty"`
		} `json:"name,omitempty"`
		Description struct {
			HTML string `json:"html,omitempty"`
			Text string `json:"text,omitempty"`
		} `json:"description,omitempty"`
	} `json:"formattedValues,omitempty"`
	HasChildren    bool     `json:"hasChildren,omitempty"`
	IsRoot         bool     `json:"isRoot,omitempty"`
	NodeID         string   `json:"nodeID,omitempty"` //nolint:tagliatelle
	URLPath        string   `json:"urlPath,omitempty"`
	URI            string   `json:"uri,omitempty"`
	WebURI         string   `json:"webUri,omitempty"`
	URIDescription string   `json:"uriDescription,omitempty"`
	Uris           nodeUris `json:"uris,omitempty"`
	ResponseLevel  string   `json:"responseLevel,omitempty"`
}

var ErrNodeWithoutChildren = fmt.Errorf("node does not contain children")

func (e *NodeDetails) GetChildren(clt client) (*ChildNode, error) {
	if e.HasChildren {
		return e.Uris.ChildNodes.Get(clt)
	}

	return nil, ErrNodeWithoutChildren
}

var ErrNodeWithoutAlbum = fmt.Errorf("node does not contain an album")

func (e *NodeDetails) GetAlbum(c client) (*Album, error) {
	if !e.HasChildren {
		return e.Uris.Album.Get(c)
	}

	return nil, ErrNodeWithoutAlbum
}

func (e *NodeDetails) NewAlbum(clt client, name string) (*Album, error) {
	// Create a new Album Node
	responseData, err := clt.PostResource(e.Uris.ChildNodes.URI, NewAPIConfig(), NewAlbumConfig(name))
	if err != nil {
		return nil, clt.Errorf("failed to create [album] resource: %w", err)
	}

	newAlbumNode, err := DecodeNode(clt, responseData)
	if err != nil {
		return nil, clt.Errorf("failed to decode [album] response: %w", err)
	}

	return newAlbumNode.GetAlbum(clt)
}

type nodeUris struct {
	FolderByID     folderByIDURI     `json:"folderByID,omitempty"` //nolint:tagliatelle
	ParentNodes    parentNodesURI    `json:"parentNodes,omitempty"`
	User           subUserURI        `json:"user,omitempty"`
	NodeCoverImage nodeCoverImageURI `json:"nodeCoverImage,omitempty"`
	HighlightImage highlightImageURI `json:"highlightImage,omitempty"`
	NodeComments   nodeCommentsURI   `json:"nodeComments,omitempty"`
	ChildNodes     childNodesURI     `json:"childNodes,omitempty"`
	MoveNodes      moveNodesURI      `json:"moveNodes,omitempty"`
	Album          albumURI          `json:"album"`
	ParentNode     parentNodeURI     `json:"parentNode"`
}
