package smugmug

import (
	"bytes"
	"crypto/md5" //nolint:gosec
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/dghubble/oauth1"
	"github.com/gabriel-vasile/mimetype"
	"gitlab.com/fteweb/smugmug/client/apiv1"
)

const fileModeUserRWGroupROtherR = 0o644

type DefaultLogger struct {
	with map[string]interface{}
}

func (dl *DefaultLogger) With(key string, value interface{}) Logger {
	logClone := *dl
	logClone.with[key] = value

	return &logClone
}

func (dl *DefaultLogger) Infof(template string, args ...interface{}) {
	if dl.with == nil {
		log.Printf("INFO:	%s", fmt.Sprintf(template, args...))

		return
	}

	withJSON, err := json.Marshal(dl.with)
	if err != nil {
		withJSON = []byte(fmt.Sprintf("Failed to marshal fields: %v", err))
	}

	log.Printf("INFO:	%s	[%s]", fmt.Sprintf(template, args...), withJSON)
}

// Generate a new instance of the OAuth Client.
//
// apiUrl = "https://secure.smugmug.com"
func New(options apiv1.ClientOptions) *Client {
	// Generate a new Client instance.
	client := new(Client)
	client.ClientOptions = options
	client.Log = &DefaultLogger{}

	return client
}

type Logger interface {
	With(string, interface{}) Logger
	Infof(string, ...interface{})
}

// Client is the main interface for the API.
type Client struct {
	// Hold the connection session.
	conn *http.Client
	// API ClientOptions options.
	ClientOptions apiv1.ClientOptions
	// Debug logging.
	Debug bool
	// LogToDisk should be set to true if you want to log API responses to disk.
	LogToDisk bool
	// ResponsePath is where the client will log the raw API response to disk.
	ResponsePath string
	// Last stores information about the previous interaction.
	Last last
	// Logging interface.
	Log Logger
}

// Copy returns a copy of the current client for templating purposes.
func (c *Client) Copy() *Client {
	newClient := *c

	return &newClient
}

// Returns the debugging state.
func (c *Client) DebugState() bool {
	return c.Debug
}

// Record and passthrough an error format.
func (c *Client) Errorf(format string, v ...interface{}) error {
	return c.Last.Errorf(format, v...)
}

// Return API Base path.
func (c *Client) Basepath(endpoint string) string {
	return c.ClientOptions.API.BasePath() + endpoint
}

// SetToken to specify a pre-generated token.
func (c *Client) SetToken(token string, secret string) {
	c.ClientOptions.SetAccessToken(token, secret)
}

// Return a formatted filepath string.
func (c *Client) GenerateResponsePath() string {
	return strings.ReplaceAll(filepath.Join(
		c.ResponsePath,
		c.Last.uri,
	), "!", string(os.PathSeparator))
}

var (
	ErrAccessTokenNotSet  = fmt.Errorf("access token not set")
	ErrAccessSecretNotSet = fmt.Errorf("access secret not set")
	ErrAPITokenNotSet     = fmt.Errorf("API token not set")
	ErrAPISecretNotSet    = fmt.Errorf("API secret not set")
)

// Verify Token is configured.
func (c *Client) ready() error {
	if c.ClientOptions.Oauth.Token.Token == "" {
		return c.Last.Error(ErrAccessTokenNotSet)
	}

	if c.ClientOptions.Oauth.Token.TokenSecret == "" {
		return c.Last.Error(ErrAccessSecretNotSet)
	}

	if c.ClientOptions.Oauth.Config.ConsumerKey == "" {
		return c.Last.Error(ErrAPITokenNotSet)
	}

	if c.ClientOptions.Oauth.Config.ConsumerSecret == "" {
		return c.Last.Error(ErrAPISecretNotSet)
	}

	return nil
}

// Connect initiates a connection that automatically authorizes each request.
func (c *Client) Connect() error {
	if err := c.ready(); err != nil {
		return c.Errorf("client not ready: %w", err)
	}

	c.conn = c.ClientOptions.Oauth.Config.Client(oauth1.NoContext, c.ClientOptions.Oauth.Token)

	return nil
}

func (c *Client) preInteract() error {
	// Check if we have available API calls.
	if err := c.ClientOptions.RateLimit.Available(); err != nil {
		return c.Last.Error(err)
	}

	// Create a connection if not already connected.
	if c.conn == nil {
		if err := c.Connect(); err != nil {
			return c.Last.Error(err)
		}
	}

	return nil
}

func (c *Client) interact(req *http.Request) ([]byte, error) {
	c.Last.request = req

	response, err := c.conn.Do(req)
	defer func() {
		if err := response.Body.Close(); err != nil {
			c.Log.Infof("Failed to close response body: %v\n", err)
		}
	}()

	if err != nil {
		return nil, c.Errorf("connection error: %w", err)
	}

	c.Last.response = response

	if err := c.ClientOptions.RateLimit.Decrement(c.ClientOptions.API.UploadDomain, req, response); err != nil {
		return nil, c.Errorf("API rate limit error: %w", err)
	}

	if c.Debug {
		c.Log.Infof("API Calls Remaining: %d\n", c.ClientOptions.RateLimit.Remaining)
		c.Log.Infof("Count Resets at %v\n", c.ClientOptions.RateLimit.Reset)
	}

	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, c.Errorf("failed to read response body: %w", err)
	}

	if err := c.logToDisk(req, contents); err != nil {
		return nil, c.Errorf("failed to log response to disk: %w", err)
	}

	return contents, nil
}

func (c *Client) logToDisk(req *http.Request, contents []byte) error {
	if !c.LogToDisk {
		return nil
	}

	// responseFolder := c.ResponsePath + strings.TrimLeft(strings.TrimRight(req.URL.Path, "/"), "/")
	responseFolder := c.GenerateResponsePath()
	c.Log.Infof("Saving response to: %s\n", responseFolder)

	// Create the export directory if it doesn't exist.
	if _, err := os.Stat(responseFolder); os.IsNotExist(err) {
		if err := os.MkdirAll(responseFolder, os.ModePerm); err != nil {
			return c.Errorf("failed to create response folder [%s]: %w", responseFolder, err)
		}
	} else if err != nil {
		return c.Errorf("failed to stat response folder [%s]: %w", responseFolder, err)
	}

	// Save the request headers.
	requestHeadersFilePath := filepath.Join(responseFolder, req.Method+".request.headers.txt")

	headersFile, err := os.Create(requestHeadersFilePath)
	defer func() {
		if err := headersFile.Close(); err != nil {
			c.Log.Infof("Failed to close request headers file [%s]: %v\n", requestHeadersFilePath, err)
		}
	}()

	if err != nil {
		return c.Errorf("failed to create request headers file [%s]: %w", requestHeadersFilePath, err)
	}

	if err = req.Header.Write(headersFile); err != nil {
		return c.Errorf("failed to write request headers file [%s]: %w", requestHeadersFilePath, err)
	}

	// Export the response to the export directory.
	responseFilePath := filepath.Join(responseFolder, req.Method+".json")
	if err = os.WriteFile(responseFilePath, contents, os.FileMode(fileModeUserRWGroupROtherR)); err != nil {
		return c.Errorf("failed to write response file [%s]: %w", responseFilePath, err)
	}

	return nil
}

var ErrNoURIProvided = fmt.Errorf("no URI provided")

// Contact the API with the specified method and return the result.
func (c *Client) apiRequest(method HTTPMethod, uri string, apiConfig *apiv1.APIConfig, apiBody []byte) ([]byte, error) {
	if err := c.preInteract(); err != nil {
		return nil, c.Errorf("API prep checks failed: %w", err)
	}

	// Check that we have a valid URI.
	if uri == "" {
		return nil, c.Last.Error(ErrNoURIProvided)
	}

	// Prepare API Request & append configuration options.
	req, err := c.ClientOptions.Query(string(c.Last.Method(method)), c.Last.URI(uri), apiConfig, apiBody)
	if err != nil {
		return nil, c.Errorf("API Query construction failed: %w", err)
	}

	if c.Debug {
		c.Log.Infof("%sing: %s", method, req.URL.String())
	}

	// Tell the API we ant a JSON response.
	req.Header.Set("Accept", "application/json")

	return c.interact(req)
}

func getMD5(file io.Reader) (string, error) {
	// Open a new hash interface to write to.
	hash := md5.New() //nolint:gosec
	// Copy the file in the hash interface and check for any error.
	if _, err := io.Copy(hash, file); err != nil {
		return "", fmt.Errorf("failed to pass data through hash: %w", err)
	}

	// Convert to a 16 byte hash, then to a string.
	return hex.EncodeToString(hash.Sum(nil)[:16]), nil
}

var ErrMissingUploadHeaders = fmt.Errorf("upload client missing headers")

// UploadResource an image or video to the API.
func (c *Client) UploadResource(sourceData io.Reader, headers *apiv1.UploadConfig) ([]byte, error) {
	if err := c.preInteract(); err != nil {
		return nil, c.Last.Error(err)
	}

	// Check that we have valid Headers.
	if !headers.Verify() {
		return nil, c.Last.Error(ErrMissingUploadHeaders)
	}

	// Prepare API Request & append configuration options.
	var fileBuffer bytes.Buffer

	fileBytes, err := ioutil.ReadAll(io.TeeReader(sourceData, &fileBuffer))
	if err != nil {
		return nil, c.Errorf("failed to read source data: %w", err)
	}

	req, err := http.NewRequest(
		string(c.Last.MethodPOST()),
		c.Last.URI(c.ClientOptions.API.UploadDomain+"/"+headers.GetFilename()),
		bytes.NewReader(fileBytes),
	)
	if err != nil {
		return nil, c.Errorf("failed to compose upload HTTP request: %w", err)
	}

	if c.Debug {
		c.Log.Infof("Uploading: %s\n", req.URL.String())
	}

	// Tell the API we ant a JSON response.
	req.Header.Set("Accept", "application/json")

	// c.Log.Infof("Filename: %s", headers.GetFilename())

	// Content-Type	The MIME type of the media	RFC 2616, section 14.17.
	contentType := mimetype.Detect(fileBuffer.Bytes())
	req.Header.Set("Content-Type", contentType.String())

	if c.Debug {
		c.Log.Infof("MIME type: %s", contentType)
	}

	// Content-MD5	The MD5 digest of the media, base64-encoded	RFC 2616, section 14.15.
	md5String, err := getMD5(bytes.NewReader(fileBuffer.Bytes()))
	if err != nil {
		return nil, c.Errorf("failed to calculate MD5 hash of source data: %w", err)
	}

	if c.Debug {
		c.Log.Infof("File MD5: %s", md5String)
	}

	req.Header.Set("Content-MD5", md5String)

	// Set user configurable headers.
	headers.SetHeaders(req)

	return c.interact(req)
}

// GetResource from the API.
func (c *Client) GetResource(uri string, apiConfig *apiv1.APIConfig) ([]byte, error) {
	return c.apiRequest(c.Last.MethodGET(), c.Last.URI(uri), apiConfig, nil)
}

// PatchResource to the API.
func (c *Client) PatchResource(uri string, apiConfig *apiv1.APIConfig, apiBody []byte) ([]byte, error) {
	return c.apiRequest(c.Last.MethodPATCH(), c.Last.URI(uri), apiConfig, apiBody)
}

var ErrInvalidItemConfig = fmt.Errorf("invalid item config")

// PostResource to the API.
func (c *Client) PostResource(uri string, apiConfig *apiv1.APIConfig, itemConfig apiv1.ItemConfig) ([]byte, error) {
	if err := itemConfig.Verify(); err != nil {
		return nil, c.Last.Error(ErrInvalidItemConfig)
	}

	apiBody, err := itemConfig.Create()
	if err != nil {
		return nil, c.Errorf("failed to create item API Body: %w", err)
	}

	return c.apiRequest(c.Last.MethodPOST(), c.Last.URI(uri), apiConfig, apiBody)
}

// DeleteResource from the API.
func (c *Client) DeleteResource(uri string, apiConfig *apiv1.APIConfig) ([]byte, error) {
	return c.apiRequest(c.Last.MethodDELETE(), c.Last.URI(uri), apiConfig, nil)
}

// OptionsResource the specified URI.
func (c *Client) OptionsResource(uri string, apiConfig *apiv1.APIConfig) ([]byte, error) {
	return c.apiRequest(c.Last.MethodOPTIONS(), c.Last.URI(uri), apiConfig, nil)
}

type HTTPMethod string

func (h HTTPMethod) String() string {
	return string(h)
}

const (
	MethodGet     HTTPMethod = http.MethodGet
	MethodHead    HTTPMethod = http.MethodHead
	MethodPost    HTTPMethod = http.MethodPost
	MethodPut     HTTPMethod = http.MethodPut
	MethodPatch   HTTPMethod = http.MethodPatch
	MethodDelete  HTTPMethod = http.MethodDelete
	MethodConnect HTTPMethod = http.MethodConnect
	MethodOptions HTTPMethod = http.MethodOptions
	MethodTrace   HTTPMethod = http.MethodTrace
)

type last struct {
	method   HTTPMethod
	uri      string
	error    error
	response *http.Response
	request  *http.Request
}

// Set the last method from the string provided and passthrough.
func (l *last) Method(method HTTPMethod) HTTPMethod {
	l.method = method

	return l.method
}

func (l *last) GetMethod() HTTPMethod {
	return l.method
}

// Set the last method to GET and passthrough.
func (l *last) MethodGET() HTTPMethod {
	l.method = http.MethodGet

	return l.method
}

// Set the last method to HEAD and passthrough.
func (l *last) MethodHEAD() HTTPMethod {
	l.method = http.MethodHead

	return l.method
}

// Set the last method to POST and passthrough.
func (l *last) MethodPOST() HTTPMethod {
	l.method = http.MethodPost

	return l.method
}

// Set the last method to PUT and passthrough.
func (l *last) MethodPUT() HTTPMethod {
	l.method = http.MethodPut

	return l.method
}

// Set the last method to PATCH and passthrough.
func (l *last) MethodPATCH() HTTPMethod {
	l.method = http.MethodPatch

	return l.method
}

// Set the last method to DELETE and passthrough.
func (l *last) MethodDELETE() HTTPMethod {
	l.method = http.MethodDelete

	return l.method
}

// Set the last method to CONNECT and passthrough.
func (l *last) MethodCONNECT() HTTPMethod {
	l.method = http.MethodConnect

	return l.method
}

// Set the last method to OPTIONS and passthrough.
func (l *last) MethodOPTIONS() HTTPMethod {
	l.method = http.MethodOptions

	return l.method
}

// Set the last method to TRACE and passthrough.
func (l *last) MethodTRACE() HTTPMethod {
	l.method = http.MethodTrace

	return l.method
}

// Set the last URI from the string provided and passthrough.
func (l *last) URI(uri string) string {
	l.uri = uri

	return l.uri
}

func (l *last) GetURI() string {
	return l.uri
}

// Set the last formatted error and passthrough.
func (l *last) Errorf(format string, v ...interface{}) error {
	l.error = fmt.Errorf(format, v...) //nolint:goerr113

	return l.error
}

// Set the last error and passthrough.
func (l *last) Error(err error) error {
	l.error = err

	return l.error
}

// Return the last error.
func (l *last) GetError() error {
	return l.error
}
