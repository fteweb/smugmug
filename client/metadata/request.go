package metadata

/*
{
    "Version":"v2",
    "Method":"GET",
	"Uri":"/api/v2"
}
*/
// request contains the API request information.
type request struct {
	// API Version
	//
	// Ex. "Version":"v2"
	Version string `json:"version,omitempty"`
	// HTTP Method
	//
	// Ex. "Method":"GET"
	Method string `json:"method,omitempty"`
	// API Path without domain
	//
	// Ex. "URI":"/api/v2"
	URI string `json:"uri,omitempty"`
}
