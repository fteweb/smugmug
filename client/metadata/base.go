package metadata

import (
	"fmt"
)

/*
{
	"Request":{  },
	"Options":{  },
	"Response":{  },
	"Code":200,
	"Message":"Ok"
}
*/

const ( // 								Error?	Description
	ErrCodeOK                  = 200 // NO		Success
	ErrCodeCreated             = 201 // NO		Successfully created a new object
	ErrCodeAccepted            = 202 // NO		Successfully started an asynchronous task
	ErrCodeOKUpperLimit        = 299 // NO		Magic Number used for upper limit of Error Checking
	ErrCodeMovedPermanently    = 301 // NO		The object has a new URI and the change is permanent
	ErrCodeMovedTemporarily    = 302 // NO		The object has a new URI but the change is temporary
	ErrCodeBadRequest          = 400 // YES		Your request contained invalid parameters or was otherwise invalid
	ErrCodeUnauthorized        = 401 // YES		Your request requires permissions you do not have
	ErrCodePaymentRequired     = 402 // YES		You must renew your paid subscription to complete this request
	ErrCodeForbidden           = 403 // YES		Your request is not allowed (for some reason other than a lack of permissions)
	ErrCodeNotFound            = 404 // YES		You do not have permissions to the object or it does not exist
	ErrCodeMethodNotAllowed    = 405 // YES		The HTTP method is not valid at this endpoint
	ErrCodeBadAccept           = 406 // YES		The Accept header in your request is not valid
	ErrCodeConflict            = 409 // YES		Your request failed due to a naming conflict
	ErrCodeTooManyRequests     = 429 // YES		You have made too many requests in too short a period of time
	ErrCodeInternalServerError = 500 // YES		You have found a bug in the SmugMug API. Please contact us.
	ErrCodeServiceUnavailable  = 503 // YES		Our servers are overloaded and cannot handle your request
)

type Base struct {
	// sub-struct
	Request request `json:"request,omitempty"`
	// sub-struct
	Options options `json:"options,omitempty"`
	*Status
	// Initialized state of this variable
	// Initialized bool `json:"-"`
}

func (b *Base) CheckErr() error {
	if err := b.Status.CheckErr(); err != nil {
		return err
	}

	if b.Options.Deprecated.Alternative != "" {
		return fmt.Errorf("%w: ALTERNATIVE: [%s]", ErrEndpointDeprecated, b.Options.Deprecated.Alternative)
	}

	return nil
}

type Status struct {
	// HTTP Response Code
	//
	// Ex. "Code":200
	Code int `json:"code,omitempty"`
	// Status Message
	//
	// Ex. "Message":"Ok"
	Message string `json:"message,omitempty"`
}

var (
	ErrResponseCodeFailed = fmt.Errorf("response code out of range")
	ErrResponseCodeMoved  = fmt.Errorf("endpoint unavailable")
	ErrEndpointDeprecated = fmt.Errorf("endpoint has been deprecated")
)

func (s *Status) CheckErr() error {
	if s.Code < ErrCodeOK || s.Code > ErrCodeOKUpperLimit {
		return fmt.Errorf("(API ERROR) %w: %d %s", ErrResponseCodeFailed, s.Code, s.Message)
	}

	if s.Code == ErrCodeMovedPermanently || s.Code == ErrCodeMovedTemporarily {
		return fmt.Errorf("(API ERROR) %w: %d %s", ErrResponseCodeMoved, s.Code, s.Message)
	}

	return nil
}
