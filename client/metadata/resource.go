package metadata

// Sub Uri template for the Response->Uris field
/*
{
	"Uri":"/api/v2/user",
	"UriDescription":"Base URI for user-related operations",
	"EndpointType":"UserBase"
}
*/
// Resource details the type of API resource that is returned.
type Resource struct {
	// URI for the Resource
	//
	// Ex. "URI":"/api/v2/user"
	URI string `json:"uri,omitempty"`
	// Additional Locators
	//
	// Ex. "Locator":"Folder,Album,Page"
	Locator string `json:"locator,omitempty"`
	// Descriptive type of the locator
	//
	// Ex. "LocatorType":"Object"
	LocatorType string `json:"locatorType,omitempty"`
	// Description of the Resource
	//
	// Ex. "URIDescription":"Base URI for user-related operations"
	URIDescription string `json:"uriDescription,omitempty"`
	// Resource Name
	//
	// Ex. "EndpointType":"UserBase"
	EndpointType string `json:"endpointType,omitempty"`
	Pages        pages  `json:"pages,omitempty"`
	DocURI       string `json:"docUri,omitempty"`
}
