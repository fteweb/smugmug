package metadata

import (
	"encoding/json"
	"fmt"
	"time"
)

/*
{
    "Uri":"/api/v2",
    "Uris":{  },
    "UriDescription":"API Version 2",
    "EndpointType":"Versionv2",
    "DocUri":"https://api.smugmug.com/api/v2/doc",
    "Timing":{  }
}
*/
// Response details the API endpoint that is returned.
type Response struct {
	// Current URI
	//
	// Ex. "URI":"/api/v2"
	URI string `json:"uri,omitempty"`
	// Endpoint type
	Locator string `json:"locator,omitempty"`
	// Endpoint generic type
	LocatorType string `json:"locatorType,omitempty"`
	// Description of the current Uri
	//
	// Ex. "URIDescription":"API Version 2"
	URIDescription string `json:"uriDescription,omitempty"`
	// Endpoint
	//
	// Ex. "EndpointType":"Versionv2"
	EndpointType string `json:"endpointType,omitempty"`
	// Link to documentation for this Uri
	//
	// Ex. "DocURI":"https://api.smugmug.com/api/v2/doc"
	DocURI string `json:"docUri,omitempty"`
	Pages  pages  `json:"pages,omitempty"`
	// sub-struct
	Timing timing `json:"timing,omitempty"`
}
type pages struct {
	Total          int    `json:"total"`
	Start          int    `json:"start"`
	Count          int    `json:"count"`
	RequestedCount int    `json:"requestedCount"`
	FirstPage      string `json:"firstPage"`
	LastPage       string `json:"lastPage"`
	PrevPage       string `json:"prevPage"`
	NextPage       string `json:"nextPage"`
}

/*
{
	"Total":{  }
}
*/
// Timing contains the response timing statistics.
type timing struct {
	// sub-struct
	Total timingTotal `json:"total,omitempty"`
}

/*
{
	"time":0.02604,
	"cycles":1,
	"objects":0
}
*/
// TimingTotal contains the statistics for the duration of the response.
type timingTotal struct {
	// Time to prep request
	//
	// Ex. "time":0.02604
	Time duration `json:"time,omitempty"`
	// Unknown reference
	//
	// Ex. "cycles":1
	Cycles int `json:"cycles,omitempty"`
	// Unknown reference
	//
	// Ex. "objects":0
	Objects int `json:"objects,omitempty"`
}

// Duration allows the Marshalling of time.Duration.
type duration struct {
	time.Duration
}

func (d duration) MarshalJSON() ([]byte, error) {
	data, err := json.Marshal(d.String())
	if err != nil {
		return nil, fmt.Errorf("failed to Marshal JSON: %w", err)
	}

	return data, nil
}

var ErrInvalidDuration = fmt.Errorf("invalid duration")

func (d *duration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return fmt.Errorf("failed to Unmarshal JSON: %w", err)
	}

	switch value := v.(type) {
	case float64:
		d.Duration = time.Duration(value)

		return nil
	case string:
		var err error
		if d.Duration, err = time.ParseDuration(value); err != nil {
			return fmt.Errorf("failed to parse duration: %w", err)
		}

		return nil
	default:
		return fmt.Errorf("%w: input not of type float64 or string", ErrInvalidDuration)
	}
}
