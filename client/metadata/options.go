package metadata

import (
	"encoding/json"
)

type options struct {
	// sub-struct
	MethodDetails methodDetails `json:"methodDetails,omitempty"`
	// HTTP Methods available on this Uri
	//
	// Ex.
	//
	// ["GET","PATCH","POST","DELETE","OPTIONS"]
	Methods []string `json:"methods,omitempty"`
	// HTTP Content Types available for this Uri
	//
	// Ex.
	//
	// [
	//
	// "application/json",
	// "application/vnd.php.serialized",
	// "application/x-msgpack",
	// "text/html",
	// "text/csv"
	//
	// ]
	MediaTypes []string `json:"mediaTypes,omitempty"`
	// Array sub-structs
	Output []parameterDetails `json:"output,omitempty"`
	// Undocumented Field
	//
	// Ex.
	//
	// ["Full","Public","Password","GrantAccess"]
	ResponseLevels []string `json:"responseLevels,omitempty"`
	// Path Parts
	Path                 []pathPart           `json:"path,omitempty"`
	Deprecated           deprecated           `json:"deprecated,omitempty"`
	ParameterDescription parameterDescription `json:"parameterDescription,omitempty"`
	Parameters           parameters           `json:"parameters,omitempty"`
	Notes                []string             `json:"notes,omitempty"`
}

type deprecated struct {
	Alternative string `json:"alternative,omitempty"`
}

type parameterDescription struct {
	Boolean string `json:"boolean,omitempty"`
	URI     string `json:"uri,omitempty"`
	Text    string `json:"text,omitempty"`
	VarChar string `json:"varchar,omitempty"`
	Array   string `json:"array,omitempty"`
	Select  string `json:"select,omitempty"`
	Integer string `json:"integer,omitempty"`
	File    string `json:"file,omitempty"`
	Decimal string `json:"decimal,omitempty"`
}

type parameters struct {
	GET     []parameterDetails `json:"get,omitempty"`
	POST    []parameterDetails `json:"post,omitempty"`
	PUT     []parameterDetails `json:"put,omitempty"`
	PATCH   []parameterDetails `json:"patch,omitempty"`
	OPTIONS []parameterDetails `json:"options,omitempty"`
	DELETE  []parameterDetails `json:"delete,omitempty"`
}

type parameterDetails struct {
	Name        string       `json:"name,omitempty"`
	Required    bool         `json:"required,omitempty"`
	ReadOnly    bool         `json:"readOnly,omitempty"`
	Default     json.Token   `json:"default,omitempty"`
	Description string       `json:"description,omitempty"`
	Type        string       `json:"type,omitempty"`
	OPTIONS     []string     `json:"options,omitempty"`
	ItemType    string       `json:"item_type,omitempty"`  //nolint:tagliatelle
	MinCount    smugMugLimit `json:"min_count,omitempty"`  //nolint:tagliatelle
	MaxCount    smugMugLimit `json:"max_count,omitempty"`  //nolint:tagliatelle
	MinChars    smugMugLimit `json:"min_chars,omitempty"`  //nolint:tagliatelle
	MaxChars    smugMugLimit `json:"max_chars,omitempty"`  //nolint:tagliatelle
	MinValue    smugMugLimit `json:"min_value,omitempty"`  //nolint:tagliatelle
	MaxValue    smugMugLimit `json:"max_value,omitempty"`  //nolint:tagliatelle
	MinLength   smugMugLimit `json:"min_length,omitempty"` //nolint:tagliatelle
	MaxLength   smugMugLimit `json:"max_length,omitempty"` //nolint:tagliatelle
	Locator     []string     `json:"locator,omitempty"`
	Deprecated  string       `json:"deprecated,omitempty"`
	Precision   int          `json:"precision,omitempty"`
	Scale       int          `json:"scale,omitempty"`
	Signed      bool         `json:"signed,omitempty"`
	Problems    []string     `json:"problems,omitempty"`
	Value       interface{}  `json:"value,omitempty"`
}

type smugMugLimit string

func (sml *smugMugLimit) UnmarshalJSON(b []byte) error {
	*sml = smugMugLimit(string(b))

	return nil
}

/*
{
	"type":"path",
	"text":"api",
	"param_name":"nickname",
	"param_value":null
}
*/
// pathPart is a component of the API Endpoint path.
type pathPart struct {
	// Type of property
	//
	// path, singleparam, etc...
	Type string `json:"type,omitempty"`
	// Path value
	Text string `json:"text,omitempty"`
	// Param key
	ParamName string `json:"param_name,omitempty"` //nolint:tagliatelle
	// Param value
	ParamValue string `json:"param_value,omitempty"` //nolint:tagliatelle
}

/*
{
	"OPTIONS":{  },
	"GET":{  }
}
*/
// MethodDetails lists the methods and options available for this endpoint.
type methodDetails struct {
	// Method Permissions
	Options methodDetailsMethod `json:"options,omitempty"`
	// Method Permissions
	Get methodDetailsMethod `json:"get,omitempty"`
	// Method Permissions
	Put methodDetailsMethod `json:"put,omitempty"`
	// Method Permissions
	Patch methodDetailsMethod `json:"patch,omitempty"`
	// Method Permissions
	Delete methodDetailsMethod `json:"delete,omitempty"`
	// Method Permissions
	Post methodDetailsMethod `json:"post,omitempty"`
}

/*
{
	"Permissions":["Read"]
}
*/
// methodDetailsMethod lists the available permissions for this method on the given endpoint.
type methodDetailsMethod struct {
	// Array of Permissions
	//
	// Ex.
	//
	// ["Read"]
	Permissions []string `json:"permissions,omitempty"`
}
