#!/bin/sh
set -e 

# apt install python3-venv python3-pip
if [ ! -d ENV ]; then
    python3 -m venv ENV
fi
chmod u+x ENV/bin/activate
./ENV/bin/activate

pip install -r requirements.txt