/*
Copyright © 2022 Frozen Tundra Entertainment devops@frozentundra.events

*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const fileModeUserRW = 0o600

var configFilePath string //nolint:gochecknoglobals

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:   "smugmug",
	Short: "A command line client for the Smugmug API",
	Long: `Smugmug allows you to interact with the Smugmug API. 
After authorizing this application using 'smugmug authorize', you can:
- browse
- download
- upload 
content in your Smugmug account`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatalf("Failed to initialize application! %v", err)
	}
}

func init() { //nolint:gochecknoinits
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	rootCmd.PersistentFlags().StringVar(
		&configFilePath,
		"config",
		"",
		"config file (default is $HOME/.config/smugmug.yaml)",
	)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if configFilePath != "" {
		// Use config file from the flag.
		viper.SetConfigFile(configFilePath)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".smugmug" (without extension).
		configFilePath = filepath.Join(home, ".config", "smugmug.yaml")
		viper.AddConfigPath(filepath.Join(home, ".config"))
		viper.SetConfigType("yaml")
		viper.SetConfigName("smugmug.yaml")
	}

	viper.SetEnvPrefix("SMUGMUG")
	viper.SetEnvKeyReplacer(strings.NewReplacer("_", "."))
	viper.AutomaticEnv() // read in environment variables that match
	viper.SetConfigPermissions(os.FileMode(fileModeUserRW))

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}

// func writeViperConfig() error {
// 	// panic: interface conversion: interface {} is nil, not map[string]interface {}
// 	if !viper.InConfig("Auth.Oauth.AccessTokenEndpoint") {
// 		delete(viper.Get("Auth.Oauth").(map[string]interface{}), "accesstokenendpoint")
// 	}
// 	if !viper.InConfig("Auth.Oauth.AuthorizeTokenEndpoint") {
// 		delete(viper.Get("Auth.Oauth").(map[string]interface{}), "authorizetokenendpoint")
// 	}
// 	if !viper.InConfig("Auth.Oauth.RequestTokenEndpoint") {
// 		delete(viper.Get("Auth.Oauth").(map[string]interface{}), "requesttokenendpoint")
// 	}
// 	if !viper.InConfig("Auth.Oauth.Domain") {
// 		delete(viper.Get("Auth.Oauth").(map[string]interface{}), "domain")
// 	}
// 	if !viper.InConfig("Auth.API.Key") {
// 		delete(viper.Get("Auth.API").(map[string]interface{}), "key")
// 	}
// 	if !viper.InConfig("Auth.API.Secret") {
// 		delete(viper.Get("Auth.API").(map[string]interface{}), "secret")
// 	}

// 	if viper.ConfigFileUsed() != "" {
// 		if err := viper.WriteConfig(); err != nil {
// 			return fmt.Errorf("Failed to write config file (%s): %w", viper.ConfigFileUsed(), err)
// 		}
// 	} else {
// 		if err := viper.WriteConfigAs(configFilePath); err != nil {
// 			return fmt.Errorf("Failed to write config file (%s): %w", configFilePath, err)
// 		}
// 	}

// 	return nil
// }
