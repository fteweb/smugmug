/*
Copyright © 2022 Frozen Tundra Entertainment devops@frozentundra.events

*/
package cmd

import (
	"fmt"
	"io"
	"log"
	"os"
	"regexp"

	"github.com/dghubble/oauth1"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	reAPIKey    = regexp.MustCompile(`^[a-zA-Z0-9]{32}$`)
	reAPISecret = regexp.MustCompile(`^[a-zA-Z0-9]{64}$`)
)

// authorizeCmd represents the authorize command.
var authorizeCmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:   "authorize",
	Short: "Authorize this application to utilize the Smugmug API",
	Long: `Interactively verifies your API Key & Secret to authorize this application to act upon you behalf.
See 'https://api.smugmug.com/api/developer/apply' to generate one.`,
	Run: func(cmd *cobra.Command, args []string) {
		authorize(cmd)
	},
}

func init() { //nolint:gochecknoinits,funlen
	rootCmd.AddCommand(authorizeCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// authorizeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// authorizeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	authorizeCmd.Flags().String("key", "", "Provide your Smugmug API Key")

	if err := viper.BindPFlag("Auth.API.Key", authorizeCmd.Flags().Lookup("key")); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}

	authorizeCmd.Flags().String("secret", "", "Provide your Smugmug API Secret")

	if err := viper.BindPFlag("Auth.API.Secret", authorizeCmd.Flags().Lookup("secret")); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}

	authorizeCmd.Flags().String("domain", "https://secure.smugmug.com", "Provide the Smugmug Oauth Domain")

	if err := viper.BindPFlag("Auth.Oauth.Domain", authorizeCmd.Flags().Lookup("domain")); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}

	authorizeCmd.Flags().String(
		"requestTokenEndpoint",
		"/services/oauth/1.0a/getRequestToken",
		"Provide the Smugmug Oauth Endpoint to request a token",
	)

	if err := viper.BindPFlag(
		"Auth.Oauth.RequestTokenEndpoint",
		authorizeCmd.Flags().Lookup("requestTokenEndpoint"),
	); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}

	authorizeCmd.Flags().String(
		"authorizeTokenEndpoint",
		"/services/oauth/1.0a/authorize",
		"Provide the Smugmug Oauth Endpoint to authorize a token",
	)

	if err := viper.BindPFlag(
		"Auth.Oauth.AuthorizeTokenEndpoint",
		authorizeCmd.Flags().Lookup("authorizeTokenEndpoint"),
	); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}

	authorizeCmd.Flags().String(
		"accessTokenEndpoint",
		"/services/oauth/1.0a/getAccessToken",
		"Provide the Smugmug Oauth Endpoint to request an access token",
	)

	if err := viper.BindPFlag(
		"Auth.Oauth.AccessTokenEndpoint",
		authorizeCmd.Flags().Lookup("accessTokenEndpoint"),
	); err != nil {
		log.Fatalf("Failed to initialize [Auth.API.Key] flag: %v", err)
	}
}

func authorize(cmd *cobra.Command) {
	apiKey := authorizeGetAPIKey(cmd)
	apiSecret := authorizeGetAPIKeySecret(cmd)
	oAuthDomain := cmd.Flag("domain").Value.String()

	smugmugEndpoint := oauth1.Endpoint{
		RequestTokenURL: oAuthDomain + cmd.Flag("requestTokenEndpoint").Value.String(),
		AuthorizeURL:    oAuthDomain + cmd.Flag("authorizeTokenEndpoint").Value.String() + "?Access=Full&Permissions=Modify",
		AccessTokenURL:  oAuthDomain + cmd.Flag("accessTokenEndpoint").Value.String(),
	}

	oauthConfig := oauth1.Config{ //nolint:exhaustruct
		ConsumerKey:    apiKey,
		ConsumerSecret: apiSecret,
		CallbackURL:    "oob",
		Endpoint:       smugmugEndpoint,
	}

	requestToken, requestSecret, err := oauthConfig.RequestToken()
	if err != nil {
		log.Printf("Failed to request API Token: %v\n", err)
		os.Exit(1)
	}

	authorizationURL, err := oauthConfig.AuthorizationURL(requestToken)
	if err != nil {
		log.Printf("Failed to generate Authorization URL: %v\n", err)
		os.Exit(1)
	}

	log.Printf("Open this URL in a web browser to authorize the API Client: %s\n", authorizationURL.String())

	accessToken, accessSecret, err := oauthConfig.AccessToken(requestToken, requestSecret, authorizeGetCode())
	if err != nil {
		log.Printf("Failed to validate Access Token: %v\n", err)
		os.Exit(1)
	}

	log.Printf("********** SMUGMUG API **********\nToken:  %s\nSecret: %s\n", accessToken, accessSecret)
	viper.Set("Auth.Access.Token", accessToken)
	viper.Set("Auth.Access.Secret", accessSecret)
}

func authorizeGetAPIKey(cmd *cobra.Command) string {
	apiKey := cmd.Flag("key").Value.String()
	if !reAPIKey.MatchString(apiKey) {
		if apiKey != "" {
			log.Printf("Invalid API Key!\n")
		}

		for {
			log.Print("Enter your SmugMug key: ")

			if _, err := fmt.Scanln(&apiKey); err != nil && err.Error() != io.ErrUnexpectedEOF.Error() {
				log.Fatalf("failed to read API Key from standard input: %v\n", err)
			}

			if reAPIKey.MatchString(apiKey) {
				viper.Set("Auth.API.Key", apiKey)

				break
			} else {
				log.Printf("Invalid API Key!\n")
			}
		}
	}

	return apiKey
}

func authorizeGetAPIKeySecret(cmd *cobra.Command) string {
	apiSecret := cmd.Flag("secret").Value.String()
	if !reAPISecret.MatchString(apiSecret) {
		if apiSecret != "" {
			log.Printf("Invalid API Secret!\n")
		}

		for {
			log.Print("Enter your SmugMug secret: ")

			if _, err := fmt.Scanln(&apiSecret); err != nil && err.Error() != io.ErrUnexpectedEOF.Error() {
				log.Fatalf("failed to read API Secret from standard input: %v\n", err)
			}

			if reAPISecret.MatchString(apiSecret) {
				viper.Set("Auth.API.Secret", apiSecret)

				break
			} else {
				log.Printf("Invalid API Secret!\n")
			}
		}
	}

	return apiSecret
}

const smugmugAuthorizationCodeLength = 6

func authorizeGetCode() string {
	var code string

	for {
		log.Print("Enter the 6 digit code: ")

		if _, err := fmt.Scanln(&code); err != nil && err.Error() != io.ErrUnexpectedEOF.Error() {
			log.Fatalf("Failed to read 6 digit code: %v\n", err)
		}

		if len(code) != smugmugAuthorizationCodeLength {
			log.Printf("Code must be %d digits, got %d\n", smugmugAuthorizationCodeLength, len(code))
		} else {
			break
		}
	}

	return code
}
