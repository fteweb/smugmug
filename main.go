/*
Copyright © 2022 Frozen Tundra Entertainment devops@frozentundra.events

*/
package main

import "gitlab.com/fteweb/smugmug/cmd"

func main() {
	cmd.Execute()
}
